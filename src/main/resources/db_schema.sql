CREATE database IF NOT EXISTS airport DEFAULT CHARACTER SET UTF8;

use airport;

DROP table IF EXISTS air_routes;
CREATE table air_routes (
routeId int not null auto_increment,
destination varchar (60) not null,
primary key (routeId)) Engine=InnoDB;

DROP table IF EXISTS aircrafts;
CREATE table aircrafts (
  aircraftId int not null auto_increment,
  aircraftName varchar (35) not null,
  passengerCapacity int not null,
  flight_id int default null,
  primary key (aircraftId)) Engine=InnoDB;

DROP table IF EXISTS flights;
CREATE table flights (
  flightId int not null auto_increment,
  orderedSeats int default null,
  departure date not null,
  air_route_id int not null,
  aircraft_id int default null,
  primary key (flightId)) Engine=InnoDB;

DROP table IF EXISTS customers;
CREATE table customers (
  customerId int not null auto_increment,
  login varchar(40) not null unique,
  password varchar(40) not null,
  customerName varchar(50) default null,
  isEnabled boolean default false,
  primary key (customerId)) Engine=InnoDB;

DROP table IF EXISTS orders;
CREATE table orders (
  orderId int not null auto_increment,
  date DATE not null,
  customer_id int not null,
  flight_id int not null,
  response_id int default null,
  primary key (orderId)) Engine=InnoDB;

DROP table IF EXISTS responses;
CREATE table responses (
  responseId int not null auto_increment,
  description varchar(200) not null,
  responseType varchar(10) not null,
  order_id int not null,
  customer_id int not null,
  primary key (responseId)) Engine=InnoDB;

DROP table IF EXISTS customers_flights;
CREATE table customers_flights (
  customer_id int not null,
  flight_id int not null) Engine=InnoDB;

ALTER TABLE flights
  ADD FOREIGN KEY (air_route_id) REFERENCES air_routes (routeId) ON DELETE CASCADE ;

ALTER TABLE flights
  ADD FOREIGN KEY (aircraft_id) REFERENCES aircrafts (aircraftId) ON DELETE CASCADE ;

ALTER TABLE orders
  ADD FOREIGN KEY (flight_id) REFERENCES flights (flightId) ON DELETE CASCADE ;

ALTER TABLE orders
  ADD FOREIGN KEY (customer_id) REFERENCES customers (customerId) ON DELETE CASCADE ;

ALTER TABLE responses
  ADD FOREIGN KEY (order_id) REFERENCES orders (orderId) ON DELETE CASCADE ;

ALTER TABLE responses
  ADD FOREIGN KEY (customer_id) REFERENCES customers (customerId) ON DELETE CASCADE ;

ALTER TABLE customers_flights
  ADD FOREIGN KEY (customer_id) REFERENCES customers (customerId) ON DELETE CASCADE ;

ALTER TABLE customers_flights
  ADD FOREIGN KEY (flight_id) REFERENCES flights (flightId)ON DELETE CASCADE ;

DELIMITER $$
CREATE PROCEDURE if_customer_exists
( IN login_input varchar(40) )
BEGIN
	DECLARE msg boolean;
  IF NOT EXISTS( SELECT * FROM customers WHERE login=login_input)
  THEN SET msg = false;
    ELSEIF EXISTS( SELECT * FROM customers WHERE login=login_input)
  THEN SET msg = true;
	END IF;
	SELECT msg AS msg;
END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE if_customer_exists_by_id
  ( IN id_input INT )
  BEGIN
    DECLARE msg boolean;
    IF NOT EXISTS( SELECT * FROM customers WHERE customerId=id_input)
    THEN SET msg = false;
    ELSEIF EXISTS( SELECT * FROM customers WHERE customerId=id_input)
      THEN SET msg = true;
    END IF;
    SELECT msg AS msg;
  END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE if_air_route_exists_by_id
  ( IN id_input INT )
  BEGIN
    DECLARE msg boolean;
    IF NOT EXISTS( SELECT * FROM air_routes WHERE routeId=id_input)
    THEN SET msg = false;
    ELSEIF EXISTS( SELECT * FROM air_routes WHERE routeId=id_input)
      THEN SET msg = true;
    END IF;
    SELECT msg AS msg;
  END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE if_aircraft_exists_by_id
  ( IN id_input INT )
  BEGIN
    DECLARE msg boolean;
    IF NOT EXISTS( SELECT * FROM aircrafts WHERE aircraftId=id_input)
    THEN SET msg = false;
    ELSEIF EXISTS( SELECT * FROM aircrafts WHERE aircraftId=id_input)
      THEN SET msg = true;
    END IF;
    SELECT msg AS msg;
  END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE if_flight_exists_by_id
  ( IN id_input INT )
  BEGIN
    DECLARE msg boolean;
    IF NOT EXISTS( SELECT * FROM flights WHERE flightId=id_input)
    THEN SET msg = false;
    ELSEIF EXISTS( SELECT * FROM flights WHERE flightId=id_input)
      THEN SET msg = true;
    END IF;
    SELECT msg AS msg;
  END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE if_order_exists_by_id
  ( IN id_input INT )
  BEGIN
    DECLARE msg boolean;
    IF NOT EXISTS( SELECT * FROM orders WHERE orderId=id_input)
    THEN SET msg = false;
    ELSEIF EXISTS( SELECT * FROM orders WHERE orderId=id_input)
      THEN SET msg = true;
    END IF;
    SELECT msg AS msg;
  END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE if_response_exists_by_id
  ( IN id_input INT )
  BEGIN
    DECLARE msg boolean;
    IF NOT EXISTS( SELECT * FROM responses WHERE responseId=id_input)
    THEN SET msg = false;
    ELSEIF EXISTS( SELECT * FROM responses WHERE responseId=id_input)
      THEN SET msg = true;
    END IF;
    SELECT msg AS msg;
  END $$
DELIMITER ;

