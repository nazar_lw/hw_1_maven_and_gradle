package com.epam.airlines_maven.controllers.browser;

import com.epam.airlines_maven.models.AirRoute;
import com.epam.airlines_maven.models.Aircraft;
import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.Flight;
import com.epam.airlines_maven.services.AirRouteService;
import com.epam.airlines_maven.services.AircraftService;
import com.epam.airlines_maven.services.CustomerService;
import com.epam.airlines_maven.services.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

@Controller
public class AdminSpringController {

    private final AirRouteService airRouteService;
    private final AircraftService aircraftService;
    private final FlightService flightService;
    private final CustomerService customerService;

    @Autowired
    public AdminSpringController(AirRouteService airRouteService, AircraftService aircraftService, FlightService flightService, CustomerService customerService) {
        this.airRouteService = airRouteService;
        this.aircraftService = aircraftService;
        this.flightService = flightService;
        this.customerService = customerService;
    }

    @GetMapping("/adminPage")
    public String adminPage() {
        return "mainAdminPage";
    }

    @GetMapping("/airRoutes")
    public String airRoutes(Model model) {
        List<AirRoute> airRoutes;
        try {
            airRoutes = airRouteService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("airRoutes", airRoutes);
        return "air_routes";
    }

    @GetMapping("/aircrafts")
    public String aircrafts(Model model) {
        List<Aircraft> aircrafts;
        try {
            aircrafts = aircraftService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("aircrafts", aircrafts);
        return "aircrafts";
    }

    @GetMapping("/flights")
    public String flights(Model model) {
        List<AirRoute> airRoutes;
        List<Aircraft> aircrafts;
        List<Flight> flights;
        try {
            airRoutes = airRouteService.getAll();
            aircrafts = aircraftService.getAll();
            flights = flightService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("airRoutes", airRoutes);
        model.addAttribute("aircrafts", aircrafts);
        model.addAttribute("flights", flights);
        return "flights";
    }

    @GetMapping("/manageCustomers")
    public String manageCustomers(Model model) {
        List<Customer> customers;
        try {
            customers = customerService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("customers", customers);
        return "manageCustomers";
    }

    @PostMapping("/addAirRoute")
    public String addAirRoute(Model model,
                              @RequestParam("destination") String destination) {
        AirRoute airRoute = AirRoute.builder().destination(destination).build();
        String resultOnSavingAirRoute = airRouteService.save(airRoute);
        model.addAttribute("resultOfSavingAirRoute", resultOnSavingAirRoute);
        List<AirRoute> airRoutes;
        try {
            airRoutes = airRouteService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("airRoutes", airRoutes);
        return "air_routes";
    }

    @PostMapping("/updateAirRoute")
    public String updateAirRoute(Model model,
                                 @RequestParam("routeId") int routeId,
                                 @RequestParam("destination") String destination) {
        AirRoute airRoute;
        List<AirRoute> airRoutes;
        try {
            airRoute = airRouteService.getById(routeId);
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        airRoute.setDestination(destination);
        String resultOnUpdatingAirRoute = airRouteService.update(airRoute);
        model.addAttribute("resultOfUpdatingAirRoute", resultOnUpdatingAirRoute);
        try {
            airRoutes = airRouteService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("airRoutes", airRoutes);
        return "air_routes";
    }

    @PostMapping("/saveAircraft")
    public String saveAircraft(Model model,
                               @RequestParam("aircraftName") String aircraftName,
                               @RequestParam("passengerCapacity") int passengerCapacity) {
        Aircraft aircraft = Aircraft.builder().aircraftName(aircraftName).
                passengerCapacity(passengerCapacity).build();
        String resultOfSavingAircraft = aircraftService.save(aircraft);
        model.addAttribute("resultOfSavingAircraft", resultOfSavingAircraft);
        List<Aircraft> aircrafts;
        try {
            aircrafts = aircraftService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("aircrafts", aircrafts);
        return "aircrafts";
    }

    @PostMapping("/updateAircraft")
    public String updateAircraft(Model model,
                                 @RequestParam("aircraftId") int aircraftId,
                                 @RequestParam("aircraftName") String aircraftName,
                                 @RequestParam("passengerCapacity") int passengerCapacity) {
        Aircraft aircraft;
        List<Aircraft> aircrafts;
        try {
            aircraft = aircraftService.getById(aircraftId);
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        aircraft.setAircraftName(aircraftName);
        aircraft.setPassengerCapacity(passengerCapacity);
        String resultOfUpdatingAircraft = aircraftService.update(aircraft);
        model.addAttribute("resultOfUpdatingAircraft", resultOfUpdatingAircraft);
        try {
            aircrafts = aircraftService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("aircrafts", aircrafts);
        return "aircrafts";
    }

    @PostMapping("/deleteAircraft")
    public String deleteAircraft(Model model,
                                 @RequestParam("aircraftId") int aircraftId) {
        String resultOfDeletingAircraft = aircraftService.delete(aircraftId);
        model.addAttribute("resultOfDeletingAircraft", resultOfDeletingAircraft);
        List<Aircraft> aircrafts;
        try {
            aircrafts = aircraftService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("aircrafts", aircrafts);
        return "aircrafts";
    }

    @PostMapping("/saveFlight")
    public String saveFlight(Model model,
                             @RequestParam("airRoute") int airRouteId,
                             @RequestParam("aircraft") int aircraftId,
                             @RequestParam("departure") Date departure) {
        AirRoute airRoute;
        Aircraft aircraft;
        List<AirRoute> airRoutes;
        List<Aircraft> aircrafts;
        List<Flight> flights;
        try {
            airRoute = airRouteService.getById(airRouteId);
            aircraft = aircraftService.getById(aircraftId);
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        Flight flight = Flight.builder().departure(departure).
                route(airRoute).aircraft(aircraft).build();
        String resultOfSavingFlight = flightService.save(flight);
        model.addAttribute("resultOfSavingFlight", resultOfSavingFlight);
        try {
            airRoutes = airRouteService.getAll();
            aircrafts = aircraftService.getAll();
            flights = flightService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("airRoutes", airRoutes);
        model.addAttribute("aircrafts", aircrafts);
        model.addAttribute("flights", flights);
        return "flights";
    }

    @PostMapping("/updateFlight")
    public String updateFlight(Model model,
                               @RequestParam("airRoute") int airRouteId,
                               @RequestParam("aircraft") int aircraftId,
                               @RequestParam("flightId") int flightId,
                               @RequestParam("orderedSeats") int orderedSeats,
                               @RequestParam("departure") Date departure) {
        AirRoute airRoute;
        Aircraft aircraft;
        Flight flight;
        List<AirRoute> airRoutes;
        List<Aircraft> aircrafts;
        List<Flight> flights;
        try {
            airRoute = airRouteService.getById(airRouteId);
            aircraft = aircraftService.getById(aircraftId);
            flight = flightService.getById(flightId);
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        flight.setAircraft(aircraft);
        flight.setRoute(airRoute);
        flight.setOrderedSeats(orderedSeats);
        flight.setDeparture(departure);
        String resultOfUpdatingFlight = flightService.update(flight);
        model.addAttribute("resultOfUpdatingFlight", resultOfUpdatingFlight);
        try {
            airRoutes = airRouteService.getAll();
            aircrafts = aircraftService.getAll();
            flights = flightService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("airRoutes", airRoutes);
        model.addAttribute("aircrafts", aircrafts);
        model.addAttribute("flights", flights);
        return "flights";
    }

    @PostMapping("/deleteFlight")
    public String deleteFlight(Model model,
                               @RequestParam("flightId") int flightId) {
        String resultOfDeletingFlight = flightService.delete(flightId);
        model.addAttribute("resultOfDeletingFlight", resultOfDeletingFlight);
        List<AirRoute> airRoutes;
        List<Aircraft> aircrafts;
        List<Flight> flights;
        try {
            airRoutes = airRouteService.getAll();
            aircrafts = aircraftService.getAll();
            flights = flightService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        model.addAttribute("airRoutes", airRoutes);
        model.addAttribute("aircrafts", aircrafts);
        model.addAttribute("flights", flights);
        return "flights";
    }

    @PostMapping("/chooseCustomer")
    public String chooseCustomer(Model model,
                                 @RequestParam("customerId") int customerId) {
        Customer customer;
        List<Customer> customers;
        try {
            customer = customerService.getById(customerId);
            customers = customerService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return "mainAdminPage";
        }
        customer.setEnabled(!customer.isEnabled());
        String resultOfChangingAccess = customerService.update(customer);
        model.addAttribute("resultOfChangingAccess", resultOfChangingAccess);
        model.addAttribute("customers", customers);
        return "manageCustomers";
    }
}
