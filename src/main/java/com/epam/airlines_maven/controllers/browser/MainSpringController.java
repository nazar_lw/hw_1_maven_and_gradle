package com.epam.airlines_maven.controllers.browser;

import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.transitionobjects.Authenticated;
import com.epam.airlines_maven.services.CustomerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.Objects;

import static com.epam.airlines_maven.utils.PropertyFileHandler.default_login;
import static com.epam.airlines_maven.utils.PropertyFileHandler.default_password;

@Controller
public class MainSpringController {

    private final CustomerService customerService;

    @Autowired
    public MainSpringController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/")
    public String index() {
//        new SQLScriptRunner().createDBifNotExists();
        return "index";
    }

    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }

    @GetMapping("/newAccount")
    public String newAccountPage() {
        return "registration";
    }

    @PostMapping("/loginCombination")
    public String loginCombination(Model model,
                                   @RequestParam("username") String username,
                                   @RequestParam("password") String password) {
        String template = "";
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            model.addAttribute("resultOfLogination", "Credentials Can not be empty Strings!!");
            return "login";
        } else if (username.equalsIgnoreCase(default_login) &&
                password.equalsIgnoreCase(default_password)) {
            template = "mainAdminPage";
        } else {
            Authenticated authenticated;
            try {
                authenticated = customerService.logIn(username, password);
            } catch (SQLException e) {
                model.addAttribute("resultOfLogination", "DB Error!");
                return "login";
            }
            if (!Objects.requireNonNull(authenticated).isAuthenticated()) {
                model.addAttribute("resultOfLogination", "Wrong Credentials!!! Try Again!");
                template = "login";
            } else if (!authenticated.getCustomer().isEnabled()) {
                model.addAttribute("resultOfLogination", "Your Account has been Suspended by the admin! !");
                template = "login";
            } else {
                model.addAttribute("customer", authenticated.getCustomer());
                template = "mainCustomerPage";
            }
        }
        return template;
    }

    @PostMapping("/saveCustomer")
    public String saveCustomer(Model model,
                               @RequestParam("login") String login,
                               @RequestParam("password") String password,
                               @RequestParam("customerName") String customerName) {
        if (StringUtils.isBlank(login) ||
                StringUtils.isBlank(password) ||
                StringUtils.isBlank(customerName)) {
            model.addAttribute("resultOfRegistration", "Credentials Can not be empty strings!!");
            return "registration";
        } else {
            Customer customer = Customer.builder().
                    login(login).
                    password(password).
                    customerName(customerName).build();
            String resultOfRegistration = customerService.save(customer);
            model.addAttribute("resultOfRegistration", resultOfRegistration);
            return "registration";
        }
    }
}
