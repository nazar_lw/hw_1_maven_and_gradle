package com.epam.airlines_maven.controllers.browser;

import com.epam.airlines_maven.models.*;
import com.epam.airlines_maven.services.CustomerService;
import com.epam.airlines_maven.services.FlightService;
import com.epam.airlines_maven.services.OrderFlightService;
import com.epam.airlines_maven.services.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.*;

@Controller
public class CustomerSpringController {

    private final CustomerService customerService;
    private final FlightService flightService;
    private final OrderFlightService orderFlightService;
    private final ResponseService responseService;

    @Autowired
    public CustomerSpringController(CustomerService customerService, FlightService flightService, OrderFlightService orderFlightService, ResponseService responseService) {
        this.customerService = customerService;
        this.flightService = flightService;
        this.orderFlightService = orderFlightService;
        this.responseService = responseService;
    }

    @PostMapping("/updateCustomer")
    public String updateCustomer(Model model,
                                 @RequestParam("customerId") int customerId,
                                 @RequestParam("customerName") String customerName) {
        Customer customer;
        try {
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("resultOfUpdatingCustomer", "DB Error!");
            return "mainCustomerPage";
        }
        Objects.requireNonNull(customer).setCustomerName(customerName);
        String resultOfUpdatingCustomer = customerService.update(customer);
        model.addAttribute("resultOfUpdatingCustomer", resultOfUpdatingCustomer);
        try {
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("resultOfUpdatingCustomer", "DB Error!");
            return "mainCustomerPage";
        }
        model.addAttribute("customer", customer);
        return "mainCustomerPage";
    }

    @PostMapping("/changePassword")
    public String changePassword(Model model,
                                 @RequestParam("customerId") int customerId,
                                 @RequestParam("oldPassword") String oldPassword,
                                 @RequestParam("newPassword") String newPassword) {
        Customer customer;
        try {
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("resultOfChangingPassword", "DB Error!");
            return "mainCustomerPage";
        }
        String resultOfChangingPassword = customerService.changePassword(customer, oldPassword, newPassword);
        model.addAttribute("resultOfChangingPassword", resultOfChangingPassword);
        model.addAttribute("customer", customer);
        return "mainCustomerPage";
    }

    @PostMapping("/deleteAccount")
    public String deleteAccount(Model model,
                                @RequestParam("customerId") int customerId) {
        String resultOfDeletingAccount = customerService.delete(customerId);
        String template = "";
        if (resultOfDeletingAccount.equals("Account was deleted")) {
            template = "index";
        } else {
            Customer customer;
            try {
                customer = customerService.getById(customerId);
            } catch (SQLException e) {
                e.printStackTrace();
                model.addAttribute("resultOfDeletingAccount", "DB Error!");
                return "mainCustomerPage";
            }
            model.addAttribute("customer", customer);
            model.addAttribute("resultOfDeletingAccount", resultOfDeletingAccount);
            template = "mainCustomerPage";
        }
        return template;
    }

    @PostMapping("/customerPage")
    public String customerPage(Model model,
                               @RequestParam("customerId") int customerId) {
        Customer customer;
        try {
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            return "login";
        }
        model.addAttribute("customer", customer);
        return "mainCustomerPage";
    }

    @PostMapping("/orders")
    public String orders(Model model,
                         @RequestParam("customerId") int customerId) {
        List<Flight> flights;
        Customer customer = null;
        List<OrderFlight> orderFlights;
        try {
            flights = flightService.getAll();
            customer = customerService.getById(customerId);
            orderFlights = orderFlightService.getAllByCustomerId(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("flights", flights);
        model.addAttribute("customer", customer);
        model.addAttribute("orderFlights", orderFlights);
        List<Response> responses = new ArrayList<>();
        model.addAttribute("responses", responses);
        return "orders";
    }

    @PostMapping("/responses")
    public String responses(Model model,
                            @RequestParam("customerId") int customerId) {
        Customer customer = null;
        List<OrderFlight> orderFlights;
        List<Response> responses;
        try {
            customer = customerService.getById(customerId);
            orderFlights = orderFlightService.getAllByCustomerId(customerId);
            responses = responseService.getAllByCustomerId(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("customer", customer);
        model.addAttribute("orderFlights", orderFlights);
        List<String> types = Arrays.asList(ResponseType.NEGATIVE.name(), ResponseType.POSITIVE.name());
        model.addAttribute("types", types);
        model.addAttribute("responses", responses);
        return "responses";
    }

    @PostMapping("/showAllMyFlights")
    public String showAllMyFlights(Model model,
                                   @RequestParam("customerId") int customerId) {
        List<Flight> flights;
        Customer customer = null;
        try {
            flights = flightService.getAllByCustomerId(customerId);
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("flights", flights);
        model.addAttribute("customer", customer);
        return "allFlightsOfCustomer";
    }

    @PostMapping("/watchResponsesOfFlight")
    public String watchResponsesOfFlight(Model model,
                                         @RequestParam("flightId") int flightId,
                                         @RequestParam("customerId") int customerId) {
        List<Response> responses;
        Customer customer = null;
        List<Flight> flights;
        List<OrderFlight> orderFlights = null;
        try {
            responses = responseService.getAllByIdFlightId(flightId);
            customer = customerService.getById(customerId);
            flights = flightService.getAll();
            orderFlights = orderFlightService.getAllByCustomerId(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("flights", flights);
        model.addAttribute("orderFlights", orderFlights);
        model.addAttribute("customer", customer);
        model.addAttribute("responses", responses);
        return "orders";
    }

    @PostMapping("/saveOrder")
    public String saveOrder(Model model,
                            @RequestParam("flightId") int flightId,
                            @RequestParam("customerId") int customerId) {
        Flight flight;
        Customer customer = null;
        List<Flight> flights;
        List<OrderFlight> orderFlights;
        try {
            flight = flightService.getById(flightId);
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        OrderFlight orderFlight = OrderFlight.builder().
                date(new java.sql.Date(Calendar.getInstance().getTime().getTime())).
                flight(flight).
                customer(customer).build();
        String resultOfSavingOrder = orderFlightService.save(orderFlight);
        model.addAttribute("resultOfSavingOrder", resultOfSavingOrder);
        try {
            flights = flightService.getAll();
            orderFlights = orderFlightService.getAllByCustomerId(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("flights", flights);
        model.addAttribute("orderFlights", orderFlights);
        model.addAttribute("customer", customer);
        List<Response> responses = new ArrayList<>();
        model.addAttribute("responses", responses);
        return "orders";
    }

    @PostMapping("/deleteOrder")
    public String deleteOrder(Model model,
                              @RequestParam("orderId") int orderId,
                              @RequestParam("customerId") int customerId) {
        Customer customer = null;
        List<Flight> flights;
        List<OrderFlight> orderFlights;
        try {
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        String resultOfDeletingOrder = orderFlightService.delete(orderId);
        model.addAttribute("resultOfDeletingOrder", resultOfDeletingOrder);
        try {
            flights = flightService.getAll();
            orderFlights = orderFlightService.getAllByCustomerId(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("flights", flights);
        model.addAttribute("orderFlights", orderFlights);
        model.addAttribute("customer", customer);
        List<Response> responses = new ArrayList<>();
        model.addAttribute("responses", responses);
        return "orders";
    }

    @PostMapping("/saveResponse")
    public String saveResponse(Model model,
                               @RequestParam("orderId") int orderId,
                               @RequestParam("customerId") int customerId,
                               @RequestParam("description") String description,
                               @RequestParam("type") String type) {
        OrderFlight orderFlight;
        Customer customer = null;
        try {
            orderFlight = orderFlightService.getById(orderId);
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        ResponseType responseType = ResponseType.valueOf(type);
        Response response = Response.builder().description(description).
                responseType(responseType).orderFlight(orderFlight).customer(customer).build();
        String resultOfSavingResponse = responseService.save(response);
        model.addAttribute("resultOfSavingResponse", resultOfSavingResponse);
        List<OrderFlight> orderFlights = null;
        List<Response> responses;
        try {
            orderFlights = orderFlightService.getAllByCustomerId(customerId);
            responses = responseService.getAllByCustomerId(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("orderFlights", orderFlights);
        model.addAttribute("customer", customer);
        List<String> types = Arrays.asList(ResponseType.NEGATIVE.name(), ResponseType.POSITIVE.name());
        model.addAttribute("types", types);
        model.addAttribute("responses", responses);
        return "responses";
    }

    @PostMapping("/updateResponse")
    public String updateResponse(Model model,
                                 @RequestParam("customerId") int customerId,
                                 @RequestParam("responseId") int responseId,
                                 @RequestParam("description") String description,
                                 @RequestParam("type") String type) {
        Response response;
        List<OrderFlight> orderFlights;
        Customer customer = null;
        List<Response> responses;
        try {
            response = responseService.getById(responseId);
            customer = customerService.getById(customerId);

        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        ResponseType responseType = ResponseType.valueOf(type);
        Objects.requireNonNull(response).setDescription(description);
        response.setResponseType(responseType);
        String resultOfUpdatingResponse = responseService.update(response);
        model.addAttribute("resultOfUpdatingResponse", resultOfUpdatingResponse);
        try {
            responses = responseService.getAllByCustomerId(customerId);
            orderFlights = orderFlightService.getAllByCustomerId(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("orderFlights", orderFlights);
        model.addAttribute("customer", customer);
        List<String> types = Arrays.asList(ResponseType.NEGATIVE.name(), ResponseType.POSITIVE.name());
        model.addAttribute("types", types);
        model.addAttribute("responses", responses);
        return "responses";
    }

    @PostMapping("/deleteResponse")
    public String deleteResponse(Model model,
                                 @RequestParam("customerId") int customerId,
                                 @RequestParam("responseId") int responseId) {
        List<OrderFlight> orderFlights;
        Customer customer = null;
        List<Response> responses;
        try {
            customer = customerService.getById(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        String resultOfDeletingResponse = responseService.delete(responseId);
        model.addAttribute("resultOfDeletingResponse", resultOfDeletingResponse);
        try {
            responses = responseService.getAllByCustomerId(customerId);
            orderFlights = orderFlightService.getAllByCustomerId(customerId);
        } catch (SQLException e) {
            e.printStackTrace();
            model.addAttribute("customer", customer);
            return "mainCustomerPage";
        }
        model.addAttribute("orderFlights", orderFlights);
        model.addAttribute("customer", customer);
        List<String> types = Arrays.asList(ResponseType.NEGATIVE.name(), ResponseType.POSITIVE.name());
        model.addAttribute("types", types);
        model.addAttribute("responses", responses);
        return "responses";
    }
}

