package com.epam.airlines_maven.controllers.desctop;

import com.epam.airlines_maven.desctopuipages.*;
import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.Flight;
import com.epam.airlines_maven.models.OrderFlight;
import com.epam.airlines_maven.models.Response;
import com.epam.airlines_maven.services.CustomerService;
import com.epam.airlines_maven.services.FlightService;
import com.epam.airlines_maven.services.OrderFlightService;
import com.epam.airlines_maven.services.ResponseService;

import java.sql.SQLException;
import java.util.List;

class CustomerController {

    private Customer customer;
    private HomePage homePage;
    private CustomerService customerService;
    private CustomerPage customerPage;
    private FlightService flightService;
    private OrderFlightService orderFlightService;
    private OrderFlightPage orderFlightPage;
    private ResponseService responseService;
    private ResponsePage responsePage;
    private FlightPage flightPage;

    CustomerController(Customer customer) {
        this.customer = customer;
        homePage = new HomePage();
        customerService = new CustomerService();
        customerPage = new CustomerPage();
        flightService = new FlightService();
        orderFlightService = new OrderFlightService();
        orderFlightPage = new OrderFlightPage();
        responseService = new ResponseService();
        responsePage = new ResponsePage();
        flightPage = new FlightPage();
    }

    public void customerDisplay() {

        int mainOption;

        do {
            mainOption = homePage.customerMainOption(customer);
            if (mainOption == 0) {
                Customer updated = customerPage.update(customer);
                homePage.reply(customerService.update(updated));
            } else if (mainOption == 1) {
                String[] passwords = customerPage.changePassword();
                homePage.reply(customerService.changePassword(customer, passwords[0], passwords[1]));
            } else if (mainOption == 2) {
                homePage.reply(customerService.delete(customer.getCustomerId()));
                return;
            } else if (mainOption == 3) {
                try {
                    List<Flight> flightList = flightService.getAll();
                    OrderFlight orderFlight = orderFlightPage.create(customer, flightList);
                    if (orderFlight != null) {
                        Flight flight = flightService.getById(orderFlight.getFlight().getFlightId());
                        List<Response> responses = responseService.getAllByIdFlightId(flight.getFlightId());
                        if (orderFlightPage.confirmFlightAfterResponses(flight, responses)) {
                            orderFlight.setFlight(flight);
                            homePage.reply(orderFlightService.save(orderFlight));
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connection to DB!");
                }
            } else if (mainOption == 4) {
                try {
                    List<OrderFlight> orderFlights =
                            orderFlightService.getAllByCustomerId(customer.getCustomerId());
                    int orderFlightIdToDelete = orderFlightPage.delete(orderFlights);
                    if (orderFlightIdToDelete != 0) {
                        homePage.reply(orderFlightService.delete(orderFlightIdToDelete));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connection to DB!");
                }
            } else if (mainOption == 5) {
                try {
                    List<OrderFlight> orderFlights =
                            orderFlightService.getAllByCustomerId(customer.getCustomerId());
                    Response response = responsePage.create(customer, orderFlights);
                    if (response != null) {
                        homePage.reply(responseService.save(response));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            } else if (mainOption == 6) {
                try {
                    List<Response> responsesOfCustomer =
                            responseService.getAllByCustomerId(customer.getCustomerId());
                    int responseIdToUpdate = responsePage.getResponseToUpdate(responsesOfCustomer);
                    if (responseIdToUpdate != 0) {
                        Response response = responseService.getById(responseIdToUpdate);
                        Response updated = responsePage.update(response);
                        homePage.reply(responseService.update(updated));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connection to DB!");
                }
            } else if (mainOption == 7) {
                try {
                    List<Flight> flightsByCustomer =
                            flightService.getAllByCustomerId(customer.getCustomerId());
                    flightPage.displayFlightsByCustomerId(flightsByCustomer);
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connection to DB!");
                }
            } else {
                return;
            }
        } while (true);
    }

}
