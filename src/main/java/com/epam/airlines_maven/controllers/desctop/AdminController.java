package com.epam.airlines_maven.controllers.desctop;

import com.epam.airlines_maven.desctopuipages.*;
import com.epam.airlines_maven.models.AirRoute;
import com.epam.airlines_maven.models.Aircraft;
import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.Flight;
import com.epam.airlines_maven.services.AirRouteService;
import com.epam.airlines_maven.services.AircraftService;
import com.epam.airlines_maven.services.CustomerService;
import com.epam.airlines_maven.services.FlightService;

import java.sql.SQLException;
import java.util.List;

class AdminController {

    private AirRouteService airRouteService;
    private AircraftService aircraftService;
    private FlightService flightService;
    private HomePage homePage;
    private AirRoutePage airRoutePage;
    private AircraftPage aircraftPage;
    private FlightPage flightPage;
    private CustomerPage customerPage;
    private CustomerService customerService;

    AdminController() {
        airRouteService = new AirRouteService();
        aircraftService = new AircraftService();
        flightService = new FlightService();
        homePage = new HomePage();
        airRoutePage = new AirRoutePage();
        aircraftPage = new AircraftPage();
        flightPage = new FlightPage();
        customerPage = new CustomerPage();
        customerService = new CustomerService();
    }

    public void adminDisplay() {

        int mainOption;

        do {
            mainOption = homePage.adminMainOption();
            if (mainOption == 0) {
                AirRoute airRoute = airRoutePage.create();
                if (airRoute.getDestination() != null) {
                    String replyOnSaveAirRoute = airRouteService.save(airRoute);
                    homePage.reply(replyOnSaveAirRoute);
                }
            } else if (mainOption == 1) {
                try {
                    List<AirRoute> airRouteList = airRouteService.getAll();
                    int airRouteIdToUpdate = airRoutePage.getAirRoute(airRouteList);
                    AirRoute airRoute = airRouteService.getById(airRouteIdToUpdate);
                    if (airRoute.getRouteId() != 0) {
                        AirRoute updated = airRoutePage.update(airRoute);
                        homePage.reply(airRouteService.update(updated));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connecting to DB!");
                }
            } else if (mainOption == 2) {
                Aircraft aircraft = aircraftPage.create();
                if (aircraft != null) {
                    aircraft.setFlight(new Flight());
                    homePage.reply(aircraftService.save(aircraft));
                }
            } else if (mainOption == 3) {
                try {
                    List<Aircraft> aircraftList = aircraftService.getAll();
                    int aircraftIdToUpdate = aircraftPage.getAircraft(aircraftList);
                    Aircraft aircraft = aircraftService.getById(aircraftIdToUpdate);
                    if (aircraft.getAircraftId() != 0) {
                        Aircraft updated = aircraftPage.update(aircraft);
                        homePage.reply(aircraftService.update(updated));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connecting to DB!");
                }
            } else if (mainOption == 4) {
                try {
                    List<Aircraft> aircraftList = aircraftService.getAll();
                    int aircraftIdToDelete = aircraftPage.deleteAircraft(aircraftList);
                    homePage.reply(aircraftService.delete(aircraftIdToDelete));
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connecting to DB!");
                }
            } else if (mainOption == 5) {
                try {
                    List<AirRoute> airRouteList = airRouteService.getAll();
                    List<Aircraft> aircraftList = aircraftService.getAll();
                    Flight flight = flightPage.create(airRouteList, aircraftList);
                    if (flight != null) {
                        homePage.reply(flightService.save(flight));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connecting to DB!");
                }
            } else if (mainOption == 6) {
                try {
                    List<AirRoute> airRouteList = airRouteService.getAll();
                    List<Aircraft> aircraftList = aircraftService.getAll();
                    List<Flight> flightList = flightService.getAll();
                    int flightIdToUpdate = flightPage.getFlight(flightList);
                    Flight flightToUpdate = flightService.getById(flightIdToUpdate);
                    if (flightToUpdate.getFlightId() != 0) {
                        Flight updated = flightPage.update(flightToUpdate, airRouteList, aircraftList);
                        AirRoute airRoute = airRouteService.getById(updated.getRoute().getRouteId());
                        Aircraft aircraft = aircraftService.getById(updated.getAircraft().getAircraftId());
                        updated.setRoute(airRoute);
                        updated.setAircraft(aircraft);
                        homePage.reply(flightService.update(updated));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connecting to DB!");
                }
            } else if (mainOption == 7) {
                try {
                    List<Flight> flightList = flightService.getAll();
                    int flightIdToDelete = flightPage.deleteFlight(flightList);
                    homePage.reply(flightService.delete(flightIdToDelete));
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connecting to DB!");
                }
            } else if (mainOption == 8) {
                try {
                    List<Customer> customers = customerService.getAll();
                    int customerIdToChaneAccess = customerPage.changeAccess(customers);
                    Customer customer = customerService.getById(customerIdToChaneAccess);
                    customer.setEnabled(!customer.isEnabled());
                    homePage.reply(customerService.update(customer));
                } catch (SQLException e) {
                    e.printStackTrace();
                    homePage.replyRed("Error connecting to DB!");
                }
            } else {
                return;
            }
        } while (true);
    }
}
