package com.epam.airlines_maven.controllers.desctop;

import com.epam.airlines_maven.desctopuipages.CustomerPage;
import com.epam.airlines_maven.desctopuipages.HomePage;
import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.transitionobjects.Authenticated;
import com.epam.airlines_maven.models.transitionobjects.UserDetails;
import com.epam.airlines_maven.services.CustomerService;

import java.sql.SQLException;
import java.util.Objects;

import static com.epam.airlines_maven.utils.PropertyFileHandler.default_login;
import static com.epam.airlines_maven.utils.PropertyFileHandler.default_password;

public class MainController {

    private HomePage homePage;
    private CustomerPage customerPage;
    private CustomerService customerService;
    private AdminController adminController;

    public MainController() {
        homePage = new HomePage();
        customerPage = new CustomerPage();
        customerService = new CustomerService();
        adminController = new AdminController();
    }

    public void display() {

        int mainOption;

        do {
            mainOption = homePage.mainOption();
            if (mainOption == 0) {
                UserDetails userDetails = homePage.logIn();
                if (userDetails.getUserLogin() != null) {
                    if (userDetails.getUserLogin().equalsIgnoreCase(default_login) &&
                            userDetails.getUserPassword().equalsIgnoreCase(default_password)) {
                        adminController.adminDisplay();
                    } else {
                        Authenticated authenticated = null;
                        try {
                            authenticated = customerService.logIn(userDetails.getUserLogin(), userDetails.getUserPassword());
                        } catch (SQLException e) {
                            e.printStackTrace();
                            homePage.reply("Error connection to DB!");
                        }
                        if (!Objects.requireNonNull(authenticated).isAuthenticated()) {
                            homePage.replyRed("Wrong credentials login or password!!!");
                        } else {
                            if (!authenticated.getCustomer().isEnabled()) {
                                homePage.reply("Your account is suspended by the Admin");
                            } else {
                                CustomerController customerController = new CustomerController(authenticated.getCustomer());
                                customerController.customerDisplay();
                            }
                        }
                    }
                }
            } else if (mainOption == 1) {
                Customer customer = customerPage.signIn();
                String replyOnCreate = customerService.save(customer);
                homePage.reply(replyOnCreate);
            } else {
                return;
            }
        } while (true);
    }

}
