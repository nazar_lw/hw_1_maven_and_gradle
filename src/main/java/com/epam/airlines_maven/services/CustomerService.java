package com.epam.airlines_maven.services;

import com.epam.airlines_maven.dao.impl.CustomerDAO;
import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.transitionobjects.Authenticated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Base64;
import java.util.List;

import static com.epam.airlines_maven.utils.PropertyFileHandler.default_login;

@Service
public class CustomerService {

    @Autowired
    private CustomerDAO customerDAO;

    public String save(Customer customer) {
        try {
            if ((customerDAO.existsByLogin(customer.getLogin())) ||
                    (customer.getLogin().equalsIgnoreCase(default_login))) {
                return "Login is not UNIQUE!";
            } else {
                String encodedPass =
                        Base64.getEncoder().encodeToString(customer.getPassword().getBytes());
                customer.setPassword(encodedPass);
                customer.setEnabled(true);
                if (customerDAO.save(customer) == 1) {
                    return "Account was created";
                } else {
                    return "Account was not created";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

    public String update(Customer customer) {
        try {
            if (customerDAO.update(customer) == 1) {
                return "Account was changed";
            } else {
                return "Account was NOT changed";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

    public Authenticated logIn(String login, String password) throws SQLException {
        Authenticated authenticated;
        Customer customer = null;

        String encodedPass = customerDAO.getPassByLogin(login);
        String decodedPass = new String(Base64.getDecoder().decode(encodedPass));
        if (decodedPass.equalsIgnoreCase(password)) {
            customer = customerDAO.getByLogin(login);
            authenticated = new Authenticated(customer, true);
        } else {
            authenticated = new Authenticated(customer, false);
        }

        return authenticated;
    }

    public String changePassword(Customer customer, String oldPass, String newPass) {
        try {
            String encodedPass = customerDAO.getPassByLogin(customer.getLogin());
            String decodedPass = new String(Base64.getDecoder().decode(encodedPass));
            if (decodedPass.equalsIgnoreCase(oldPass)) {
                String newDecodedPass =
                        Base64.getEncoder().encodeToString(newPass.getBytes());
                customer.setPassword(newDecodedPass);
                if (customerDAO.changePassword(customer) == 1) {
                    return "Password was changed";
                } else {
                    return "Password was NOT changed";
                }
            } else {
                return "Passwords DO NOT match!";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error connection to DB!";
        }
    }

    public List<Customer> getAll() throws SQLException {
        return customerDAO.getAll();
    }

    public Customer getById(int customerId) throws SQLException {
        return customerDAO.getById(customerId);
    }

    public String delete(int customerId) {
        try {
            customerDAO.delete(customerId);
            boolean exists = customerDAO.existsById(customerId);
            if (exists) {
                return "Account was not deleted";
            } else {
                return "Account was deleted";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error connection to DB!";
        }
    }
}
