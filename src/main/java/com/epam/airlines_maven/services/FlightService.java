package com.epam.airlines_maven.services;

import com.epam.airlines_maven.dao.impl.FlightDAO;
import com.epam.airlines_maven.models.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class FlightService {

    @Autowired
    private FlightDAO flightDAO;

    public List<Flight> getAll() throws SQLException {
        return flightDAO.getAll();
    }

    public Flight getById(int id) throws SQLException {
        return flightDAO.getById(id);
    }

    public List<Flight> getAllByCustomerId(int customerId) throws SQLException {
        return flightDAO.getAllByCustomerId(customerId);
    }

    public String save(Flight flight) {
        try {
            if (flightDAO.save(flight) == 1) {
                return "Flight was saved";
            } else {
                return "Flight was not saved";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

    public String update(Flight flight) {
        try {
            if (flightDAO.update(flight) == 1) {
                return "Flight was updated";
            } else {
                return "Flight was not updated";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

    public String delete(int flightId) {
        try {
            flightDAO.delete(flightId);
            boolean exists = flightDAO.existsById(flightId);
            if (exists) {
                return "Flight was not deleted";
            } else {
                return "Flight was deleted";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

}
