package com.epam.airlines_maven.services;

import com.epam.airlines_maven.dao.impl.AirRouteDAO;
import com.epam.airlines_maven.models.AirRoute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class AirRouteService {

    @Autowired
    private AirRouteDAO airRouteDAO;

    public List<AirRoute> getAll() throws SQLException {
        return airRouteDAO.getAll();
    }

    public AirRoute getById(int id) throws SQLException {
        return airRouteDAO.getById(id);
    }

    public String save(AirRoute airRoute) {
        try {
            if (airRouteDAO.save(airRoute) == 1) {
                return "Air route was saved";
            } else {
                return "Air route was not saved";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error connection to DB!";
        }
    }

    public String update(AirRoute airRoute) {
        try {
            if (airRouteDAO.update(airRoute) == 1) {
                return "Air route was updated";
            } else {
                return "Air route was not updated";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error connection to DB!";
        }
    }

    public String delete(int id) {
        try {
            airRouteDAO.delete(id);
            boolean exists = airRouteDAO.existsById(id);
            if (exists) {
                return "Air route was not deleted";
            } else {
                return "Air route was deleted";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error connection to DB!";
        }
    }
}
