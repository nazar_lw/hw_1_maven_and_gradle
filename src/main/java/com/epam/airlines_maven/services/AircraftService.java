package com.epam.airlines_maven.services;

import com.epam.airlines_maven.dao.impl.AircraftDAO;
import com.epam.airlines_maven.dao.impl.FlightDAO;
import com.epam.airlines_maven.models.Aircraft;
import com.epam.airlines_maven.models.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class AircraftService {

    @Autowired
    private AircraftDAO aircraftDAO;
    @Autowired
    private FlightDAO flightDAO;

    public List<Aircraft> getAll() throws SQLException {
        return aircraftDAO.getAll();
    }

    public Aircraft getById(int id) throws SQLException {
        return aircraftDAO.getById(id);
    }

    public String save(Aircraft aircraft) {
        try {
            if (aircraftDAO.save(aircraft) == 1) {
                return "Aircraft was saved";
            } else {
                return "Aircraft was not saved";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error connection to DB!";
        }
    }

    public String update(Aircraft aircraft) {
        try {
            if (aircraftDAO.update(aircraft) == 1) {
                return "Aircraft was updated";
            } else {
                return "Aircraft was not updated";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error connection to DB!";
        }
    }

    public String delete(int id) {
        try {
            Flight flight = flightDAO.getByAircraftId(id);
            if (flight.getFlightId() != 0) {
                return "AIRCRAFT is RESERVED to flight: " +
                        flight.getFlightId() + "- assign another aircraft to this flight!";
            } else {
                aircraftDAO.delete(id);
                boolean exists = aircraftDAO.existsById(id);
                if (exists) {
                    return "Aircraft was not deleted";
                } else {
                    return "Aircraft was deleted";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error connection to DB!";
        }
    }
}
