package com.epam.airlines_maven.services;

import com.epam.airlines_maven.dao.impl.FlightDAO;
import com.epam.airlines_maven.dao.impl.OrderFlightDAO;
import com.epam.airlines_maven.models.Flight;
import com.epam.airlines_maven.models.OrderFlight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class OrderFlightService {

    @Autowired
    private OrderFlightDAO orderFlightDAO;
    @Autowired
    private FlightDAO flightDAO;

    public List<OrderFlight> getAll() throws SQLException {
        return orderFlightDAO.getAll();
    }

    public OrderFlight getById(int id) throws SQLException {
        return orderFlightDAO.getById(id);
    }

    public List<OrderFlight> getAllByCustomerId(int customerId) throws SQLException {
        return orderFlightDAO.getAllByCustomerId(customerId);
    }

    public String save(OrderFlight orderFlight) {
        try {
            Flight flight = orderFlight.getFlight();
            boolean addSeat = flight.addOrderedSeat();
            if (!addSeat) {
                return "No free seats available!";
            } else if (flight.getDeparture().before(orderFlight.getDate())) {
                return "The date is past due!";
            } else {
                flightDAO.update(flight);
                orderFlight.setFlight(flight);
                if (orderFlightDAO.save(orderFlight) == 1) {
                    return "Order of flight was saved";
                } else {
                    return "Order of flight was not saved";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

    public String update(OrderFlight orderFlight) {
        try {
            if (orderFlightDAO.update(orderFlight) == 1) {
                return "Order of flight was updated";
            } else {
                return "Order of flight was not updated";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

    public String delete(int id) {
        try {
            OrderFlight orderFlight = orderFlightDAO.getById(id);
            Flight flight = flightDAO.getById(orderFlight.getFlight().getFlightId());
            flight.subtractOrderedSeat();
            flightDAO.update(flight);
            orderFlightDAO.delete(id);
            boolean exists = orderFlightDAO.existsById(id);
            if (exists) {
                return "Order of flight was not deleted";
            } else {
                return "Order of flight was deleted";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "DB Error!";
        }
    }
}
