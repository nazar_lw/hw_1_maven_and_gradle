package com.epam.airlines_maven.services;

import com.epam.airlines_maven.dao.impl.ResponseDAO;
import com.epam.airlines_maven.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class ResponseService {

    @Autowired
    private ResponseDAO responseDAO;

    public List<Response> getAll() throws SQLException {
        return responseDAO.getAll();
    }

    public Response getById(int id) throws SQLException {
        return responseDAO.getById(id);
    }

    public List<Response> getAllByIdFlightId(int flightId) throws SQLException {
        return responseDAO.getAllByFlightId(flightId);
    }

    public List<Response> getAllByCustomerId(int customerId) throws SQLException {
        return responseDAO.getAllByCustomerId(customerId);
    }

    public String save(Response response) {
        try {
            if (responseDAO.save(response) == 1) {
                return "Response was saved";
            } else {
                return "Response was not saved";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

    public String update(Response response) {
        try {
            if (responseDAO.update(response) == 1) {
                return "Response was updated";
            } else {
                return "Response was not updated";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

    public String delete(int id) {
        try {
            responseDAO.delete(id);
            boolean exists = responseDAO.existsById(id);
            if (exists) {
                return "Response was not deleted";
            } else {
                return "Response was deleted";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error!";
        }
    }

}
