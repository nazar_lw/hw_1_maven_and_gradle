package com.epam.airlines_maven.desctopuipages;

import com.epam.airlines_maven.models.AirRoute;

import javax.swing.*;
import java.util.List;

public class AirRoutePage extends AbstractPage {

    public AirRoutePage() { }

    public AirRoute create(){
        AirRoute airRoute = new AirRoute();
        JTextField destinationField = new JTextField(40);
        JPanel panel = new JPanel();
        panel.add(new JLabel("set destination:"));
        panel.add(destinationField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please enter data to air root:", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            airRoute.setDestination(destinationField.getText());
        }
        return airRoute;
    }

    public AirRoute update(AirRoute airRoute) {
        JTextField destinationField = new JTextField(String.valueOf(airRoute.getDestination()),40);
        JPanel panel = new JPanel();
        panel.add(new JLabel("set destination:"));
        panel.add(destinationField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please update data of air root "+airRoute.toString(), JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            airRoute.setDestination(destinationField.getText());
        }
        return airRoute;
    }

    public int getAirRoute(List<AirRoute> airRouteList) {

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        airRouteList.forEach(route -> panel.add(new JLabel(route.toString())));
        JTextField airRouteIdField = new JTextField(5);
        panel.add(new JLabel("-----------------------------------------------"));
        panel.add(new JLabel("set id of Air Route from the list above:"));
        panel.add(airRouteIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please choose Air route from the List:", JOptionPane.OK_CANCEL_OPTION);
        int airRouteId= 0;
        if (result == JOptionPane.OK_OPTION) {
            try{
                airRouteId = Integer.parseInt(airRouteIdField.getText());
            } catch (NumberFormatException e){
                replyRed("Wrong input Air Route id!");
            }
        }
        return airRouteId;
    }

}
