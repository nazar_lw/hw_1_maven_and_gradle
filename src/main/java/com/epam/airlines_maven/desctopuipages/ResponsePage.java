package com.epam.airlines_maven.desctopuipages;

import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.OrderFlight;
import com.epam.airlines_maven.models.Response;
import com.epam.airlines_maven.models.ResponseType;

import javax.swing.*;
import java.util.List;

public class ResponsePage extends AbstractPage {

    public Response create(Customer customer, List<OrderFlight> orderFlights){
        Response response = null;
        JTextField descriptionField = new JTextField(40);
        JTextField responseTypeField = new JTextField(20);
        JTextField orderFlightIdField = new JTextField(20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("The list of Orders"));
        orderFlights.forEach(orderFlight ->
                panel.add(new JLabel(orderFlight.toStringWithoutCustomerAndResponses())));
        panel.add(new JLabel("description:"));
        panel.add(descriptionField);
        panel.add(new JLabel("response Type in format 'NEGATIVE' or 'POSITIVE':"));
        panel.add(responseTypeField);
        panel.add(new JLabel("choose OrderFlight id from the list above:"));
        panel.add(orderFlightIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Enter the data of a Response", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                OrderFlight orderFlight= OrderFlight.builder().orderId(Integer.parseInt(orderFlightIdField.getText())).build();
                response = Response.builder().
                        description(descriptionField.getText()).
                        responseType(ResponseType.valueOf(responseTypeField.getText())).
                        orderFlight(orderFlight).
                        customer(customer).build();
            }catch (IllegalArgumentException e){
                replyRed("Wrong input - data format- Order Flight id or Response type");
            }
        }
        return response;
    }

    public int getResponseToUpdate(List<Response> responses){
        JTextField responseIdField = new JTextField(40);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("The list of your Responses:"));
        responses.forEach(response ->
                panel.add(new JLabel("Response Id-"+response.getResponseId()+
                        ", Description- "+response.getDescription()+
                        ", Type-"+ response.getResponseType()+
                        ", Order date-"+response.getOrderFlight().getDate()+
                        ", Fligth departure-"+response.getOrderFlight().getFlight().getDeparture()+
                        ", Destination-"+response.getOrderFlight().getFlight().getRoute().getDestination())));
        panel.add(new JLabel("----------------------------"));
        panel.add(new JLabel("Choose response to update from List "));
        panel.add(responseIdField);
        int responseIdToUpdate = 0;
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Set the Response, you want to update", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                responseIdToUpdate = Integer.parseInt(responseIdField.getText());
            }catch (IllegalArgumentException e){
                replyRed("Wrong input - data format- Response type");
            }
        }
        return responseIdToUpdate;
    }

    public Response update(Response response){

        JTextField descriptionField = new JTextField(response.getDescription(),40);
        JTextField responseTypeField = new JTextField(response.getResponseType().name(),20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("description:"));
        panel.add(descriptionField);
        panel.add(new JLabel("response Type in format 'NEGATIVE' or 'POSITIVE':"));
        panel.add(responseTypeField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Enter the data of a Response", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                response.setDescription(descriptionField.getText());
                response.setResponseType(ResponseType.valueOf(responseTypeField.getText()));
            }catch (IllegalArgumentException e){
                replyRed("Wrong input - data format- Response type");
            }
        }
        return response;
    }

}
