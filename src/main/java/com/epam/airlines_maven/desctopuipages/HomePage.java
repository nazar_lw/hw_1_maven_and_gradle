package com.epam.airlines_maven.desctopuipages;

import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.transitionobjects.UserDetails;

import javax.swing.*;

public class HomePage extends AbstractPage {

    public HomePage() { }

    public int mainOption(){

        String[] options = {"Log In", "Create Account", "Cancel"};
        return JOptionPane.showOptionDialog(null,
                "Choose the option",
                "Click a button",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                options,
                options[0]);
    }

    public int adminMainOption(){
        String[] options = {
                "Add new Air route",
                "Update Air route",
                "Add new Aircraft",
                "Update Aircraft",
                "Delete Aircraft",
                "Add new Flight",
                "Update Flight",
                "Delete Flight",
                "Manage Customers",
                "Cancel"};
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        return JOptionPane.showOptionDialog(null,
                "Hello, default Admin!! Choose the option:",
                "Click a button",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                options,
                options[0]);
    }

    public int customerMainOption(Customer customer){
        String[] options = {
                "Update Account",
                "Change password",
                "Delete Account",
                "Add new Order Flight",
                "Delete Order Flight",
                "Add Response",
                "Update Response",
                "Get all my flights",
                "Cancel"};
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        return JOptionPane.showOptionDialog(null,
                "Hello, "+customer.getCustomerName()+"!! Choose the option:",
                "Click a button",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                options,
                options[0]);
    }

    public UserDetails logIn(){
        UserDetails user = new UserDetails();
        JTextField loginField = new JTextField(20);
        JTextField passwordField = new JPasswordField(20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("login:"));
        panel.add(loginField);
        panel.add(new JLabel("password:"));
        panel.add(passwordField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Enter your login and password", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            user.setUserLogin(loginField.getText());
            user.setUserPassword(passwordField.getText());
        }
        return user;
    }

}
