package com.epam.airlines_maven.desctopuipages;

import com.epam.airlines_maven.models.Aircraft;

import javax.swing.*;
import java.sql.SQLException;
import java.util.List;

public class AircraftPage extends AbstractPage {

    public AircraftPage() { }

    public Aircraft create(){

        Aircraft aircraft = null;
        JTextField aircraftNameField = new JTextField(20);
        JTextField passengerCapacityField = new JTextField(20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("aircraft name:"));
        panel.add(aircraftNameField);
        panel.add(new JLabel("passenger capacity:"));
        panel.add(passengerCapacityField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Enter the data of an aircraft", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                aircraft = Aircraft.builder().
                        aircraftName(aircraftNameField.getText()).
                        passengerCapacity(Integer.parseInt(passengerCapacityField.getText())).
                        build();
            }catch (NumberFormatException e){
                replyRed("Wrong input - passenger capacity");
            }
        }
        return aircraft;
    }

    public int getAircraft(List<Aircraft> aircraftList)  {

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        aircraftList.forEach(aircraft1 -> panel.add(new JLabel(aircraft1.toString())));
        panel.add(new JLabel("-----------------------------------------------"));
        JTextField aircraftIdField = new JTextField(5);
        panel.add(new JLabel("set id of Aircraft from the list above:"));
        panel.add(aircraftIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please choose Aircraft from the List:", JOptionPane.OK_CANCEL_OPTION);
        int aircraftId=0;
        if (result == JOptionPane.OK_OPTION) {
            try{
                aircraftId = Integer.parseInt(aircraftIdField.getText());
            }catch (NumberFormatException e){
                replyRed("Wrong input Aircraft id!");
            }
        }
        return aircraftId;
    }

    public int deleteAircraft(List<Aircraft> aircraftList) throws SQLException {

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        aircraftList.forEach(aircraft -> panel.add(new JLabel(aircraft.toString())));
        panel.add(new JLabel("-----------------------------------------------"));
        JTextField aircraftIdField = new JTextField(5);
        panel.add(new JLabel("set id of Aircraft from the list above:"));
        panel.add(aircraftIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please choose Aircraft from the List TO DELETE:", JOptionPane.OK_CANCEL_OPTION);
        int aircraftIdToDelete = 0;
        if (result == JOptionPane.OK_OPTION) {
            try{
                aircraftIdToDelete =  Integer.parseInt(aircraftIdField.getText());
            }catch (NumberFormatException e){
                replyRed("Wrong input Aircraft id!");
            }
        }
        return aircraftIdToDelete;
    }

    public Aircraft update(Aircraft aircraft){

        JTextField aircraftNameField = new JTextField(String.valueOf(aircraft.getAircraftName()),20);
        JTextField passengerCapacityField = new JTextField(String.valueOf(aircraft.getPassengerCapacity()),20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("Aircraft to update: "+aircraft.toString()));
        panel.add(new JLabel("aircraft name:"));
        panel.add(aircraftNameField);
        panel.add(new JLabel("passenger capacity:"));
        panel.add(passengerCapacityField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please update data of an aircraft",
                JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                aircraft.setAircraftName(aircraftNameField.getText());
                aircraft.setPassengerCapacity(Integer.parseInt(passengerCapacityField.getText()));
            }catch (NumberFormatException e){
                replyRed("Wrong input - passenger capacity");
            }
        }
        return aircraft;
    }
}
