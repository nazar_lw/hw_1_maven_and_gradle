package com.epam.airlines_maven.desctopuipages;

import com.epam.airlines_maven.models.Customer;

import javax.swing.*;
import java.util.List;

public class CustomerPage extends AbstractPage {

    public CustomerPage() { }

    public Customer signIn(){
        Customer customer = null;
        JTextField loginField = new JTextField(20);
        JTextField passwordField = new JPasswordField(20);
        JTextField nameField = new JTextField(20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("login:"));
        panel.add(loginField);
        panel.add(new JLabel("password:"));
        panel.add(passwordField);
        panel.add(new JLabel("name:"));
        panel.add(nameField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Enter your data", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            customer= Customer.builder().login(loginField.getText()).
                    password(passwordField.getText()).
                    isEnabled(true).
                    customerName(nameField.getText()).build();
        }
        return customer;
    }

    public Customer update(Customer customer){
        JTextField nameField = new JTextField(String.valueOf(customer.getCustomerName()), 40);
        JPanel panel = new JPanel();
        panel.add(new JLabel("set New Name:"));
        panel.add(nameField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please update data of customer: "+customer.toString(),
                JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            customer.setCustomerName(nameField.getText());
        }
        return customer;
    }

    public String[] changePassword(){
        String[] passwords = new String[2];
        JTextField oldPassField = new JTextField(20);
        JTextField newPassField = new JTextField(20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("set old password:"));
        panel.add(oldPassField);
        panel.add(new JLabel("set new password:"));
        panel.add(newPassField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Enter old and new passwords:", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            passwords[0] = oldPassField.getText();
            passwords[1] = newPassField.getText();
        }
        return passwords;
    }

    public int changeAccess(List<Customer> customers) {

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        customers.forEach(customer -> panel.add(new JLabel(customer.toString())));
        panel.add(new JLabel("-----------------------------------------------"));
        JTextField customerIdField = new JTextField(5);
        panel.add(new JLabel("set id of Customer from the list above:"));
        panel.add(customerIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please choose Customer from the List TO CHANGE access:", JOptionPane.OK_CANCEL_OPTION);
        int customerIdToChangeAccess = 0;
        if (result == JOptionPane.OK_OPTION) {
            try{
                customerIdToChangeAccess = Integer.parseInt(customerIdField.getText());
            }catch (NumberFormatException e){
                replyRed("Wrong input Customer id!");
            }
        }
        return customerIdToChangeAccess;
    }

}
