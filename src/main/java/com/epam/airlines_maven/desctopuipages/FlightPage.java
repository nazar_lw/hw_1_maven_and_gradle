package com.epam.airlines_maven.desctopuipages;

import com.epam.airlines_maven.models.AirRoute;
import com.epam.airlines_maven.models.Aircraft;
import com.epam.airlines_maven.models.Flight;

import javax.swing.*;
import java.sql.Date;
import java.util.List;

public class FlightPage extends AbstractPage {

    public FlightPage() { }

    public Flight create(List<AirRoute> airRouteList, List<Aircraft> aircraftList) {

        Flight flight = null;
//        JTextField orderedSeatsField = new JTextField();
        JTextField departureField = new JTextField();
        JTextField airRouteIdField = new JTextField();
        JTextField aircraftIdField = new JTextField();
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("The list of air routes:"));
        airRouteList.forEach(airRoute -> panel.add(new JLabel(airRoute.toString())));
        panel.add(new JLabel("-----------------------------------------------"));
        panel.add(new JLabel("The list of aircrafts"));
        aircraftList.forEach(aircraft -> panel.add(new JLabel(aircraft.toString())));
        panel.add(new JLabel("-----------------------------------------------"));
//        panel.add(new JLabel("ordered seats:"));
//        panel.add(orderedSeatsField);
        panel.add(new JLabel("departureField date in format 'yyyy-mm-dd':"));
        panel.add(departureField);
        panel.add(new JLabel("choose Air route id from the list above:"));
        panel.add(airRouteIdField);
        panel.add(new JLabel("choose Aircraft id from the list above::"));
        panel.add(aircraftIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Enter the data of a Flight", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                AirRoute airRoute = AirRoute.builder().routeId(Integer.parseInt(airRouteIdField.getText())).build();
                Aircraft aircraft = Aircraft.builder().aircraftId(Integer.parseInt(aircraftIdField.getText())).build();
                flight= Flight.builder().
                        orderedSeats(0).
                        departure(Date.valueOf(departureField.getText())).
                        route(airRoute).
                        aircraft(aircraft).build();
            }catch (IllegalArgumentException e){
                replyRed("Wrong input - data format- Date");
            }
        }
        return flight;
    }

    public int getFlight(List<Flight> flightList) {

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        flightList.forEach(flight1 -> panel.add(new JLabel(flight1.toStringWithAircraft())));
        panel.add(new JLabel("-----------------------------------------------"));
        JTextField flightIdField = new JTextField();
        panel.add(new JLabel("set id of Flight from the list above:"));
        panel.add(flightIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please choose Flight from the List:", JOptionPane.OK_CANCEL_OPTION);
        int flightId=0;
        if (result == JOptionPane.OK_OPTION) {
            try{
                flightId = Integer.parseInt(flightIdField.getText());
            }catch (NumberFormatException e){
                replyRed("Wrong input Flight id!");
            }
        }
        return flightId;
    }

    public Flight update(Flight flight, List<AirRoute> airRouteList, List<Aircraft> aircraftList)  {

        JTextField orderedSeatsField = new JTextField(String.valueOf(flight.getOrderedSeats()));
        JTextField departureField = new JTextField(String.valueOf(flight.getDeparture()));
        JTextField airRouteIdField = new JTextField(String.valueOf(flight.getRoute().getRouteId()));
        JTextField aircraftIdField = new JTextField(String.valueOf(flight.getAircraft().getAircraftId()));
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("The list of air routes:"));
        airRouteList.forEach(airRoute -> panel.add(new JLabel(airRoute.toString())));
        panel.add(new JLabel("-----------------------------------------------"));
        panel.add(new JLabel("The list of aircrafts:"));
        aircraftList.forEach(aircraft -> panel.add(new JLabel(aircraft.toString())));
        panel.add(new JLabel("-----------------------------------------------"));
        panel.add(new JLabel("ordered seats:"));
        panel.add(orderedSeatsField);
        panel.add(new JLabel("departureField date in format 'yyyy-mm-dd':"));
        panel.add(departureField);
        panel.add(new JLabel("choose Air route id from the list above:"));
        panel.add(airRouteIdField);
        panel.add(new JLabel("choose Aircraft id from the list above::"));
        panel.add(aircraftIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please Update the data of a Flight", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                AirRoute airRoute = AirRoute.builder().routeId(Integer.parseInt(airRouteIdField.getText())).build();
                Aircraft aircraft = Aircraft.builder().aircraftId(Integer.parseInt(aircraftIdField.getText())).build();
                flight.setOrderedSeats(Integer.parseInt(orderedSeatsField.getText()));
                flight.setDeparture(Date.valueOf(departureField.getText()));
                flight.setRoute(airRoute);
                flight.setAircraft(aircraft);
            }catch (NumberFormatException e){
                replyRed("Wrong input - data format- Date");
            }
        }
        return flight;
    }

    public int deleteFlight(List<Flight> flightList) {

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        flightList.forEach(flight -> panel.add(new JLabel(flight.toStringWithAircraft())));
        panel.add(new JLabel("-----------------------------------------------"));
        JTextField flightIdField = new JTextField(5);
        panel.add(new JLabel("set id of Flight from the list above:"));
        panel.add(flightIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please choose Flight from the List TO DELETE:", JOptionPane.OK_CANCEL_OPTION);
        int flightIdToDelete = 0;
        if (result == JOptionPane.OK_OPTION) {
            flightIdToDelete=  Integer.parseInt(flightIdField.getText());
        }
        return flightIdToDelete;
    }

    public void displayFlightsByCustomerId(List<Flight> flightList){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        flightList.forEach(flight ->
                panel.add(new JLabel("Flight id- "+flight.getFlightId()+
                ", Ordered seats- "+flight.getOrderedSeats()+
                ", Departure- "+flight.getDeparture()+
                ", Destination- "+flight.getRoute().getDestination()+
                ", Aircraft name- "+flight.getAircraft().getAircraftName())));
         JOptionPane.showConfirmDialog(null, panel,
                "Please choose Flight from the List TO DELETE:", JOptionPane.DEFAULT_OPTION);
    }
}
