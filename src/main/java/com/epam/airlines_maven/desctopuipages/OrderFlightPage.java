package com.epam.airlines_maven.desctopuipages;

import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.models.Flight;
import com.epam.airlines_maven.models.OrderFlight;
import com.epam.airlines_maven.models.Response;

import javax.swing.*;
import java.util.Calendar;
import java.util.List;

public class OrderFlightPage extends AbstractPage {

    public OrderFlight create(Customer customer, List<Flight> flightList){
        OrderFlight orderFlight = null;
        JTextField flightIdField = new JTextField(20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(new JLabel("The list of Flights"));
        flightList.forEach(flight -> panel.add(new JLabel(flight.toStringWithAircraft())));

        panel.add(flightIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please choose Flight id from the list below:", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                Flight flight = Flight.builder().flightId(Integer.parseInt(flightIdField.getText())).build();
                orderFlight = OrderFlight.builder().
                        date(new java.sql.Date(Calendar.getInstance().getTime().getTime())).
                        flight(flight).
                        customer(customer).build();
            }catch (IllegalArgumentException e){
                replyRed("Wrong input - data format- Flight id");
            }
        }
        return orderFlight;
    }

    public boolean confirmFlightAfterResponses(Flight flight, List<Response> flightResponses){

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("The list of responses of Flight: "+flight.toString()));
        panel.add(new JLabel("---------------------------------------------"));
        flightResponses.forEach(response ->
                panel.add(new JLabel(response.toStringMainData())));
        panel.add(new JLabel("---------------------------------------------"));
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Do you want to choose this Flight taking into account its responses:", JOptionPane.YES_NO_CANCEL_OPTION);
        boolean option = false;
        if (result == JOptionPane.YES_OPTION) {
            try {
                option= true;
            }catch (IllegalArgumentException e){
                replyRed("Wrong input - data format- OrderFlight id");
            }
        }else {
            return false;
        }
        return option;
    }

    public int delete(List<OrderFlight> orderFlightListList){

        JTextField orderFlightIdField = new JTextField(20);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(new JLabel("The list of your Orders"));
        orderFlightListList.forEach(orderFlight ->
                panel.add(new JLabel(orderFlight.toStringWithoutCustomerAndResponses())));
        panel.add(orderFlightIdField);
        int result = JOptionPane.showConfirmDialog(null, panel,
                "Please choose Flight id from the list below:", JOptionPane.OK_CANCEL_OPTION);
        int orderFlightIdToDelete=0;
        if (result == JOptionPane.OK_OPTION) {
            try {
                orderFlightIdToDelete = Integer.parseInt(orderFlightIdField.getText());
            }catch (IllegalArgumentException e){
                replyRed("Wrong input - data format- OrderFlight id");
            }
        }
        return orderFlightIdToDelete;
    }
}
