package com.epam.airlines_maven.dao.impl;

import com.epam.airlines_maven.dao.AbstractDAO;
import com.epam.airlines_maven.models.AirRoute;
import com.epam.airlines_maven.models.Aircraft;
import com.epam.airlines_maven.models.Flight;
import com.epam.airlines_maven.utils.ConnectionConfiguration;
import org.springframework.context.annotation.Configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class FlightDAO implements AbstractDAO<Flight> {

    private static final String GET_ALL = "SELECT * FROM flights left join aircrafts " +
            "on aircraftId= aircraft_id left join air_routes on routeId=air_route_id";

    private static final String GET_BY_ID = "SELECT * FROM flights left join aircrafts " +
            "on aircraftId= aircraft_id left join air_routes on routeId= air_route_id WHERE flightId=?";

    private static final String GET_BY_AIRCRAFT_ID = "SELECT * FROM flights left join aircrafts " +
            "on aircraftId= aircraft_id left join air_routes on routeId= air_route_id WHERE aircraft_Id=?";

    private static final String GET_ALL_BY_CUSTOMER_ID = "select flightId, departure, destination, " +
            "orderedSeats, aircraftName from flights left join" +
            " air_routes on routeId=air_route_id left join aircrafts on aircraftId=aircraft_id" +
            " inner join customers_flights on flightId=customers_flights.flight_id inner" +
            " join customers on customerId=customer_id where customerId=?";

    private static final String EXISTS_BY_ID = "CALL if_flight_exists_by_id (?)";

    private static final String SAVE = "INSERT flights (flightId, orderedSeats, departure, air_route_id, aircraft_id) " +
            "VALUES (null, ?, ?, ?, ?)";

    private static final String UPDATE = "UPDATE flights SET orderedSeats=?, departure=?, air_route_id=?, aircraft_id=? " +
            "WHERE flightId=?";

    private static final String DELETE = "DELETE FROM flights WHERE flightId=?";

    @Override
    public List<Flight> getAll() throws SQLException {
        List<Flight> flights = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL)) {
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    AirRoute airRoute = AirRoute.builder().
                            routeId(resultSet.getInt("routeId")).
                            destination(resultSet.getString("destination")).build();
                    Aircraft aircraft = Aircraft.builder().
                            aircraftId(resultSet.getInt("aircraftId")).
                            aircraftName(resultSet.getString("aircraftName")).
                            passengerCapacity(resultSet.getInt("passengerCapacity")).
                            build();
                    Flight flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            orderedSeats(resultSet.getInt("orderedSeats")).
                            departure(resultSet.getDate("departure")).
                            aircraft(aircraft).
                            route(airRoute).build();
                    flights.add(flight);
                }
            }
        }
        return flights;
    }

    public List<Flight> getAllByCustomerId(int customerId) throws SQLException {
        List<Flight> flights = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL_BY_CUSTOMER_ID)) {
            ps.setInt(1, customerId);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    AirRoute airRoute = AirRoute.builder().
                            destination(resultSet.getString("destination")).build();
                    Aircraft aircraft = Aircraft.builder().
                            aircraftName(resultSet.getString("aircraftName")).
                            build();
                    Flight flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            orderedSeats(resultSet.getInt("orderedSeats")).
                            departure(resultSet.getDate("departure")).
                            aircraft(aircraft).
                            route(airRoute).build();
                    flights.add(flight);
                }
            }
        }
        return flights;
    }

    @Override
    public Flight getById(int id) throws SQLException {
        Flight flight = new Flight();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    AirRoute airRoute = AirRoute.builder().
                            routeId(resultSet.getInt("routeId")).
                            destination(resultSet.getString("destination")).build();
                    Aircraft aircraft = Aircraft.builder().
                            aircraftId(resultSet.getInt("aircraftId")).
                            aircraftName(resultSet.getString("aircraftName")).
                            passengerCapacity(resultSet.getInt("passengerCapacity")).build();
                    flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            orderedSeats(resultSet.getInt("orderedSeats")).
                            departure(resultSet.getDate("departure")).
                            route(airRoute).
                            aircraft(aircraft).
                            build();
                }
            }
        }
        return flight;
    }

    public Flight getByAircraftId(int id) throws SQLException {
        Flight flight = new Flight();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_AIRCRAFT_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    AirRoute airRoute = AirRoute.builder().
                            routeId(resultSet.getInt("routeId")).
                            destination(resultSet.getString("destination")).build();
                    Aircraft aircraft = Aircraft.builder().
                            aircraftId(resultSet.getInt("aircraftId")).
                            aircraftName(resultSet.getString("aircraftName")).
                            passengerCapacity(resultSet.getInt("passengerCapacity")).build();
                    flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            orderedSeats(resultSet.getInt("orderedSeats")).
                            departure(resultSet.getDate("departure")).
                            route(airRoute).
                            aircraft(aircraft).
                            build();
                }
            }
        }
        return flight;
    }

    public boolean existsById(int idInput) throws SQLException {
        boolean message = false;
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(EXISTS_BY_ID)) {
            ps.setInt(1, idInput);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    message = resultSet.getBoolean("msg");
                }
            }
        }
        return message;
    }

    @Override
    public int save(Flight flight) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(SAVE)) {
            ps.setInt(1, flight.getOrderedSeats());
            ps.setDate(2, flight.getDeparture());
            ps.setInt(3, flight.getRoute().getRouteId());
            ps.setInt(4, flight.getAircraft().getAircraftId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Flight flight) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(UPDATE)) {
            ps.setInt(1, flight.getOrderedSeats());
            ps.setDate(2, flight.getDeparture());
            ps.setInt(3, flight.getRoute().getRouteId());
            ps.setInt(4, flight.getAircraft().getAircraftId());
            ps.setInt(5, flight.getFlightId());
            return ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(DELETE)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
    }
}
