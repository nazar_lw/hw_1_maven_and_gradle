package com.epam.airlines_maven.dao.impl;

import com.epam.airlines_maven.dao.AbstractDAO;
import com.epam.airlines_maven.models.*;
import com.epam.airlines_maven.utils.ConnectionConfiguration;
import org.springframework.context.annotation.Configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class ResponseDAO implements AbstractDAO<Response> {

    private static final String GET_ALL = "SELECT * FROM responses inner join orders " +
            "on orderId=order_id inner join flights on orderId=order_id " +
            "inner join customers on orderId=order_id ";

    private static final String GET_ALL_BY_CUSTOMER_ID = "SELECT * FROM responses inner join customers " +
            "on customerId=customer_id inner join orders on orderId=order_id " +
            " inner join flights on flightId= flight_id inner join air_routes on routeId = air_route_id where customerId=?";


    private static final String GET_BY_ID = "SELECT * FROM responses inner join customers " +
            "on customerId=customer_id inner join orders on orderId=order_id " +
            "inner join flights on flightId= flight_id where responseId=? ";

    private static final String GET_ALL_BY_FLIGHT_ID = "select responseId, description, responseType " +
            "from responses left join orders on orderId=order_id left join" +
            " flights on flightId= flight_id where flightId=?";

    private static final String EXISTS_BY_ID = "CALL if_response_exists_by_id (?)";

    private static final String SAVE = "INSERT responses (responseId, description, responseType, " +
            "order_id, customer_id) VALUES (null, ?, ?, ?, ?)";

    private static final String UPDATE = "UPDATE responses SET description=?, responseType=?, order_id=?, " +
            " customer_id=? WHERE responseId=?";

    private static final String DELETE = "DELETE FROM responses WHERE responseId=?";

    @Override
    public List<Response> getAll() throws SQLException {
        List<Response> responses = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL)) {
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    Flight flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            departure(resultSet.getDate("departure")).build();
                    OrderFlight orderFlight = OrderFlight.builder().
                            orderId(resultSet.getInt("orderId")).
                            date(resultSet.getDate("date")).
                            flight(flight).build();
                    Customer customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).build();
                    Response response = Response.builder().
                            responseId(resultSet.getInt("responseId")).
                            description(resultSet.getString("description")).
                            responseType(ResponseType.valueOf(resultSet.getString("responseType"))).
                            orderFlight(orderFlight).
                            customer(customer).
                            build();
                    responses.add(response);
                }
            }
        }
        return responses;
    }

    public List<Response> getAllByCustomerId(int customerId) throws SQLException {
        List<Response> responses = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL_BY_CUSTOMER_ID)) {
            ps.setInt(1, customerId);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    AirRoute airRoute = AirRoute.builder().
                            routeId(resultSet.getInt("routeId")).
                            destination(resultSet.getString("destination")).build();
                    Flight flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            departure(resultSet.getDate("departure")).
                            route(airRoute).
                            build();
                    OrderFlight orderFlight = OrderFlight.builder().
                            orderId(resultSet.getInt("orderId")).
                            date(resultSet.getDate("date")).
                            flight(flight).build();
                    Customer customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).build();
                    Response response = Response.builder().
                            responseId(resultSet.getInt("responseId")).
                            description(resultSet.getString("description")).
                            responseType(ResponseType.valueOf(resultSet.getString("responseType"))).
                            orderFlight(orderFlight).
                            customer(customer).
                            build();
                    responses.add(response);
                }
            }
        }
        return responses;
    }

    @Override
    public Response getById(int id) throws SQLException {
        Response response = new Response();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    Flight flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            departure(resultSet.getDate("departure")).build();
                    OrderFlight orderFlight = OrderFlight.builder().
                            orderId(resultSet.getInt("orderId")).
                            date(resultSet.getDate("date")).
                            flight(flight).build();
                    Customer customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).build();
                    response = Response.builder().
                            responseId(resultSet.getInt("responseId")).
                            description(resultSet.getString("description")).
                            responseType(ResponseType.valueOf(resultSet.getString("responseType"))).
                            orderFlight(orderFlight).
                            customer(customer).
                            build();
                }
            }
        }
        return response;
    }

    public List<Response> getAllByFlightId(int flightId) throws SQLException {
        List<Response> responses = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL_BY_FLIGHT_ID)) {
            ps.setInt(1, flightId);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    Response response = Response.builder().
                            responseId(resultSet.getInt("responseId")).
                            description(resultSet.getString("description")).
                            responseType(ResponseType.valueOf(resultSet.getString("responseType"))).
                            build();
                    responses.add(response);
                }
            }
        }
        return responses;
    }

    public boolean existsById(int idInput) throws SQLException {
        boolean message = false;
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(EXISTS_BY_ID)) {
            ps.setInt(1, idInput);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    message = resultSet.getBoolean("msg");
                }
            }
        }
        return message;
    }

    @Override
    public int save(Response response) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(SAVE)) {
            ps.setString(1, response.getDescription());
            ps.setString(2, String.valueOf(response.getResponseType()));
            ps.setInt(3, response.getOrderFlight().getOrderId());
            ps.setInt(4, response.getCustomer().getCustomerId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Response response) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(UPDATE)) {
            ps.setString(1, response.getDescription());
            ps.setString(2, String.valueOf(response.getResponseType()));
            ps.setInt(3, response.getOrderFlight().getOrderId());
            ps.setInt(4, response.getCustomer().getCustomerId());
            ps.setInt(5, response.getResponseId());
            return ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(DELETE)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
    }
}
