package com.epam.airlines_maven.dao.impl;

import com.epam.airlines_maven.dao.AbstractDAO;
import com.epam.airlines_maven.models.AirRoute;
import com.epam.airlines_maven.utils.ConnectionConfiguration;
import org.springframework.context.annotation.Configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class AirRouteDAO implements AbstractDAO<AirRoute> {

    private static final String GET_ALL = "SELECT * FROM air_routes";
    private static final String GET_BY_ID = "SELECT * FROM air_routes WHERE routeId=?";
    private static final String EXISTS_BY_ID = "CALL if_air_route_exists_by_id (?)";
    private static final String SAVE = "INSERT air_routes (routeId, destination) VALUES (null, ?)";
    private static final String UPDATE = "UPDATE air_routes SET destination=? WHERE routeId=?";
    private static final String DELETE = "DELETE FROM air_routes WHERE routeId=?";

    @Override
    public List<AirRoute> getAll() throws SQLException {
        List<AirRoute> airRoutes = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL)) {
             try(ResultSet resultSet = ps.executeQuery()) {
                 while (resultSet.next()) {
                     AirRoute airRoute = AirRoute.builder().
                             routeId(resultSet.getInt("routeId")).
                             destination(resultSet.getString("destination")).build();
                     airRoutes.add(airRoute);
                 }
             }
        }
        return airRoutes;
    }

    @Override
    public AirRoute getById(int id) throws SQLException {
        AirRoute airRoute = new AirRoute();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    airRoute = AirRoute.builder().
                            routeId(resultSet.getInt("routeId")).
                            destination(resultSet.getString("destination")).build();
                }
            }
        }
        return airRoute;
    }

    public boolean existsById(int idInput) throws SQLException {
        boolean message = false;
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(EXISTS_BY_ID)) {
            ps.setInt(1, idInput);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    message = resultSet.getBoolean("msg");
                }
            }
        }
        return message;
    }

    @Override
    public int save(AirRoute airRoute) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(SAVE)) {
            ps.setString(1, airRoute.getDestination());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(AirRoute airRoute) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(UPDATE)) {
            ps.setString(1, airRoute.getDestination());
            ps.setInt(2, airRoute.getRouteId());
            return ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(DELETE)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
    }
}
