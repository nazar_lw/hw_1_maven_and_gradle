package com.epam.airlines_maven.dao.impl;

import com.epam.airlines_maven.dao.AbstractDAO;
import com.epam.airlines_maven.models.Aircraft;
import com.epam.airlines_maven.utils.ConnectionConfiguration;
import org.springframework.context.annotation.Configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class AircraftDAO implements AbstractDAO<Aircraft> {

    private static final String GET_ALL = "SELECT * FROM aircrafts ";

    private static final String GET_BY_ID = "SELECT * FROM aircrafts WHERE aircraftId=?";

    private static final String GET_BY_FLIGHT_ID = "SELECT *FROM aircrafts inner JOIN " +
            "flights on aircraftId= aircraft_id WHERE flightId=?";

    private static final String EXISTS_BY_ID = "CALL if_aircraft_exists_by_id (?)";

    private static final String SAVE = "INSERT aircrafts (aircraftId, aircraftName, passengerCapacity)" +
            " VALUES (null, ?, ?)";

    private static final String UPDATE = "UPDATE aircrafts SET aircraftName=?, passengerCapacity=?" +
            " WHERE aircraftId=?";

    private static final String DELETE = "DELETE FROM aircrafts WHERE aircraftId=?";

    @Override
    public List<Aircraft> getAll() throws SQLException {
        List<Aircraft> aircrafts = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL)) {
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    Aircraft aircraft = Aircraft.builder().
                            aircraftId(resultSet.getInt("aircraftId")).
                            aircraftName(resultSet.getString("aircraftName")).
                            passengerCapacity(resultSet.getInt("passengerCapacity")).
                            build();
                    aircrafts.add(aircraft);
                }
            }
        }
        return aircrafts;
    }

    @Override
    public Aircraft getById(int id) throws SQLException {
        Aircraft aircraft = new Aircraft();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    aircraft = Aircraft.builder().
                            aircraftId(resultSet.getInt("aircraftId")).
                            aircraftName(resultSet.getString("aircraftName")).
                            passengerCapacity(resultSet.getInt("passengerCapacity")).
                            build();
                }
            }
        }
        return aircraft;
    }

    public Aircraft getByFlightId(int id) throws SQLException {
        Aircraft aircraft = new Aircraft();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_FLIGHT_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    aircraft.setAircraftId(resultSet.getInt("aircraftId"));
                    aircraft.setAircraftName(resultSet.getString("aircraftName"));
                    aircraft.setPassengerCapacity(resultSet.getInt("passengerCapacity"));
                }
            }
        }
        return aircraft;
    }

    public boolean existsById(int idInput) throws SQLException {
        boolean message = false;
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(EXISTS_BY_ID)) {
            ps.setInt(1, idInput);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    message = resultSet.getBoolean("msg");
                }
            }
        }
        return message;
    }

    @Override
    public int save(Aircraft aircraft) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(SAVE)) {
            ps.setString(1, aircraft.getAircraftName());
            ps.setInt(2, aircraft.getPassengerCapacity());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Aircraft aircraft) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(UPDATE)) {
            ps.setString(1, aircraft.getAircraftName());
            ps.setInt(2, aircraft.getPassengerCapacity());
            ps.setInt(3, aircraft.getAircraftId());
            return ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(DELETE)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
    }
}
