package com.epam.airlines_maven.dao.impl;

import com.epam.airlines_maven.dao.AbstractDAO;
import com.epam.airlines_maven.models.*;
import com.epam.airlines_maven.utils.ConnectionConfiguration;
import org.springframework.context.annotation.Configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class OrderFlightDAO implements AbstractDAO<OrderFlight> {

    private static final String GET_ALL = "SELECT * FROM orders left join flights " +
            "on flightId=flight_id left join air_routes on routeId=air_route_id left join customers on customerId=customer_id " +
            "left join responses on orderId=order_id ";

    private static final String GET_BY_ID = "SELECT * FROM orders left join flights " +
            "on flightId=flight_id left join air_routes on routeId=air_route_id left join customers on customerId=customer_id " +
            "left join responses on orderId=order_id " +
            "WHERE orderId=?";

    private static final String GET_ALL_BY_CUSTOMER_ID = "SELECT * FROM orders left join flights " +
            "on flightId=flight_id left join air_routes on routeId=air_route_id left join customers on customerId=customer_id " +
            "left join responses on orderId=order_id " +
            "WHERE orders.customer_id=?";

    private static final String EXISTS_BY_ID = "CALL if_order_exists_by_id (?)";

    private static final String SAVE = "INSERT orders (orderId, date, customer_id, flight_id, response_id)" +
            " VALUES (null, ?, ?, ?, ?)";

    private static final String SAVE_CUSTOMER_FLIGHT = "INSERT customers_flights " +
            "(customer_id, flight_id) VALUES (?, ?)";

    private static final String UPDATE = "UPDATE orders SET date=?, customer_id=?, flight_id=?, " +
            " response_id=? WHERE orderId=?";

    private static final String DELETE = "DELETE FROM orders WHERE orderId=?";

    @Override
    public List<OrderFlight> getAll() throws SQLException {
        List<OrderFlight> orderFlights = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL)) {
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    AirRoute airRoute = AirRoute.builder().
                            routeId(resultSet.getInt("routeId")).
                            destination(resultSet.getString("destination")).build();
                    Flight flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            orderedSeats(resultSet.getInt("orderedSeats")).
                            departure(resultSet.getDate("departure")).
                            route(airRoute).build();
                    Customer customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).build();
                    Response response = null;
                    if (resultSet.getString("responseType") != null) {
                        response = Response.builder().
                                responseId(resultSet.getInt("responseId")).
                                description(resultSet.getString("description")).
                                responseType(ResponseType.valueOf(resultSet.getString("responseType")))
                                .build();
                    }
                    OrderFlight orderFlight = OrderFlight.builder().
                            orderId(resultSet.getInt("orderId")).
                            date(resultSet.getDate("date")).
                            flight(flight).
                            customer(customer).
                            response(response).
                            build();
                    orderFlights.add(orderFlight);
                }
            }
        }
        return orderFlights;
    }

    @Override
    public OrderFlight getById(int id) throws SQLException {
        OrderFlight orderFlight = new OrderFlight();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    AirRoute airRoute = AirRoute.builder().
                            routeId(resultSet.getInt("routeId")).
                            destination(resultSet.getString("destination")).build();
                    Flight flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            orderedSeats(resultSet.getInt("orderedSeats")).
                            departure(resultSet.getDate("departure")).
                            route(airRoute).build();
                    Customer customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).build();
                    Response response = null;
                    if (resultSet.getString("responseType") != null) {
                        response = Response.builder().
                                responseId(resultSet.getInt("responseId")).
                                description(resultSet.getString("description")).
                                responseType(ResponseType.valueOf(resultSet.getString("responseType")))
                                .build();
                    }
                    orderFlight = OrderFlight.builder().
                            orderId(resultSet.getInt("orderId")).
                            date(resultSet.getDate("date")).
                            flight(flight).
                            customer(customer).
                            response(response).
                            build();
                }
            }
        }
        return orderFlight;
    }

    public List<OrderFlight> getAllByCustomerId(int customerId) throws SQLException {
        List<OrderFlight> orderFlights = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL_BY_CUSTOMER_ID)) {
            ps.setInt(1, customerId);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    AirRoute airRoute = AirRoute.builder().
                            routeId(resultSet.getInt("routeId")).
                            destination(resultSet.getString("destination")).build();
                    Flight flight = Flight.builder().
                            flightId(resultSet.getInt("flightId")).
                            orderedSeats(resultSet.getInt("orderedSeats")).
                            departure(resultSet.getDate("departure")).
                            route(airRoute).build();
                    Customer customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).build();
                    Response response = null;
                    if (resultSet.getString("responseType") != null) {
                        response = Response.builder().
                                responseId(resultSet.getInt("responseId")).
                                description(resultSet.getString("description")).
                                responseType(ResponseType.valueOf(resultSet.getString("responseType"))).
                                build();
                    }
                    OrderFlight orderFlight = OrderFlight.builder().
                            orderId(resultSet.getInt("orderId")).
                            date(resultSet.getDate("date")).
                            flight(flight).
                            customer(customer).
                            response(response).
                            build();
                    orderFlights.add(orderFlight);
                }
            }
        }
        return orderFlights;
    }

    public boolean existsById(int idInput) throws SQLException {
        boolean message = false;
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(EXISTS_BY_ID)) {
            ps.setInt(1, idInput);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    message = resultSet.getBoolean("msg");
                }
            }
        }
        return message;
    }

    @Override
    public int save(OrderFlight orderFlight) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(SAVE);
             PreparedStatement ps1 = ConnectionConfiguration.getConnection().prepareStatement(SAVE_CUSTOMER_FLIGHT)) {
            ps.setDate(1, orderFlight.getDate());
            ps.setInt(2, orderFlight.getCustomer().getCustomerId());
            ps.setInt(3, orderFlight.getFlight().getFlightId());
            if (orderFlight.getResponse() == null) {
                ps.setInt(4, 0);
            } else {
                ps.setInt(4, orderFlight.getResponse().getResponseId());
            }
            ps1.setInt(1, orderFlight.getCustomer().getCustomerId());
            ps1.setInt(2, orderFlight.getFlight().getFlightId());
            int st = ps.executeUpdate();
            int st1 = ps1.executeUpdate();
            if ((st == 1) && (st1 == 1)) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    @Override
    public int update(OrderFlight orderFlight) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(UPDATE)) {
            ps.setDate(1, orderFlight.getDate());
            ps.setInt(2, orderFlight.getCustomer().getCustomerId());
            ps.setInt(3, orderFlight.getFlight().getFlightId());
            ps.setInt(4, orderFlight.getResponse().getResponseId());
            ps.setInt(5, orderFlight.getOrderId());
            return ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(DELETE)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
    }
}
