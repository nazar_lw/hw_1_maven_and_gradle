package com.epam.airlines_maven.dao.impl;

import com.epam.airlines_maven.dao.AbstractDAO;
import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.utils.ConnectionConfiguration;
import org.springframework.context.annotation.Configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class CustomerDAO implements AbstractDAO<Customer> {

    private static final String GET_ALL = "SELECT * FROM customers";

    private static final String GET_BY_ID = "SELECT * FROM customers where customerId=?";

    private static final String GET_BY_LOGIN = "SELECT * FROM customers where login=?";

    private static final String EXISTS_BY_LOGIN = "CALL if_customer_exists (?)";

    private static final String EXISTS_BY_ID = "CALL if_customer_exists_by_id (?)";

    private static final String GET_PASS_BY_LOGIN = "SELECT password from customers WHERE login=?";

    private static final String CHANGE_PASSWORD = "UPDATE customers SET password=? WHERE customerId=?";

    private static final String SAVE = "INSERT customers (customerId, login, password, " +
            "customerName, isEnabled) VALUES (null, ?, ?, ?, ?)";

    private static final String UPDATE = "UPDATE customers  SET customerName=?, " +
            " isEnabled=? WHERE customerId=?";

    private static final String DELETE = "DELETE FROM customers WHERE customerId=?";

    @Override
    public List<Customer> getAll() throws SQLException {
        List<Customer> customers = new ArrayList<>();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_ALL)) {
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    Customer customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            login(resultSet.getString("login")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).
                            build();
                    customers.add(customer);
                }
            }
        }
        return customers;
    }

    @Override
    public Customer getById(int id) throws SQLException {
        Customer customer = new Customer();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            login(resultSet.getString("login")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).
                            build();
                }
            }
        }
        return customer;
    }

    public Customer getByLogin(String login) throws SQLException {
        Customer customer = new Customer();
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_BY_LOGIN)) {
            ps.setString(1, login);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    customer = Customer.builder().
                            customerId(resultSet.getInt("customerId")).
                            login(resultSet.getString("login")).
                            customerName(resultSet.getString("customerName")).
                            isEnabled(resultSet.getBoolean("isEnabled")).
                            build();
                }
            }
        }
        return customer;
    }

    public boolean existsByLogin(String loginInput) throws SQLException {
        boolean message = false;
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(EXISTS_BY_LOGIN)) {
            ps.setString(1, loginInput);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    message = resultSet.getBoolean("msg");
                }
            }
        }
        return message;
    }

    public boolean existsById(int idInput) throws SQLException {
        boolean message = false;
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(EXISTS_BY_ID)) {
            ps.setInt(1, idInput);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    message = resultSet.getBoolean("msg");
                }
            }
        }
        return message;
    }

    public String getPassByLogin(String login) throws SQLException {
        String pass = "";
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(GET_PASS_BY_LOGIN)) {
            ps.setString(1, login);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    pass = resultSet.getString("password");
                }
            }
        }
        return pass;
    }

    @Override
    public int save(Customer customer) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(SAVE)) {
            ps.setString(1, customer.getLogin());
            ps.setString(2, customer.getPassword());
            ps.setString(3, customer.getCustomerName());
            ps.setBoolean(4, customer.isEnabled());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Customer customer) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(UPDATE)) {
            ps.setString(1, customer.getCustomerName());
            ps.setBoolean(2, customer.isEnabled());
            ps.setInt(3, customer.getCustomerId());
            return ps.executeUpdate();
        }
    }

    public int changePassword(Customer customer) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(CHANGE_PASSWORD)) {
            ps.setString(1, customer.getPassword());
            ps.setInt(2, customer.getCustomerId());
            return ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement ps = ConnectionConfiguration.getConnection().prepareStatement(DELETE)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
    }
}
