package com.epam.airlines_maven.dao;

import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public interface AbstractDAO<T> {

    List<T> getAll() throws SQLException;

    T getById(int id) throws SQLException;

    int save(T object) throws SQLException;

    int update(T object) throws SQLException;

    void delete(int id) throws SQLException;

}
