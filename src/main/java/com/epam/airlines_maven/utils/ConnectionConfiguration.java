package com.epam.airlines_maven.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.epam.airlines_maven.utils.PropertyFileHandler.*;

public class ConnectionConfiguration implements AutoCloseable {

    private static String url = String.format(url_model,
            db_port,
            db_name,
            db_url_additional,
            db_url_codding);

    private static Connection connection = null;

    public ConnectionConfiguration() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(url, db_user, db_password);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    @Override
    public void close() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
