package com.epam.airlines_maven.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyFileHandler {

    public static String url_model;
    public static String url_model_without_db;
    public static String db_name;
    public static String db_user;
    public static String db_password;
    public static String db_port;
    public static String db_url_additional;
    public static String db_url_codding;
    public static String default_login;
    public static String default_password;
    public static String db_schema_path;


    static {
        url_model = getProps().getProperty("url_model");
        url_model_without_db = getProps().getProperty("url_model_without_db");
        db_name = getProps().getProperty("db_name");
        db_user = getProps().getProperty("db_user");
        db_password = getProps().getProperty("db_password");
        db_port = getProps().getProperty("db_port");
        db_url_additional = getProps().getProperty("db_url_additional");
        db_url_codding = getProps().getProperty("db_url_codding");
        default_login = getProps().getProperty("defaultLogin");
        default_password = getProps().getProperty("defaultPassword");
        db_schema_path = getProps().getProperty("db_schema");
    }

    private static Properties getProps() {
        Properties props = null;
        try (InputStream input =
                     PropertyFileHandler.class.getResourceAsStream("/constants.properties")) {
            props = new Properties();
            props.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return props;
    }
}