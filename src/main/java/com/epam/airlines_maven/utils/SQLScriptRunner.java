package com.epam.airlines_maven.utils;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;

import static com.epam.airlines_maven.utils.PropertyFileHandler.*;

public class SQLScriptRunner {

    private boolean checkIfDatabaseExists() {
        boolean ifExists = true;
        String url = String.format(url_model,
                db_port,
                db_name,
                db_url_additional,
                db_url_codding);
        try (Connection connection = DriverManager.getConnection(url, db_user, db_password)) {
            System.out.println(connection.getMetaData().getCatalogs());
        } catch (SQLSyntaxErrorException e) {
            ifExists = false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ifExists;
    }

    private void runScript() {
        String url = String.format(url_model_without_db,
                db_port,
                db_url_additional,
                db_url_codding);
        try {
            Connection connection =
                    DriverManager.getConnection(url,
                            db_user, db_password);
            ScriptRunner sr = new ScriptRunner(connection);
            try (InputStream inputStream = getClass().getResourceAsStream("/db_schema.sql");
                 BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                sr.runScript(reader);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createDBifNotExists() {
        if (!checkIfDatabaseExists()) {
            runScript();
        } else {
            System.out.println("Database- " + db_name + " already exists");
        }
    }
}
