package com.epam.airlines_maven;

import com.epam.airlines_maven.controllers.desctop.MainController;
import com.epam.airlines_maven.utils.SQLScriptRunner;

public class MainClass {
    public static void main(String[] args) {
//        new SQLScriptRunner().createDBifNotExists();
        new MainController().display();
    }
}
