package com.epam.airlines_maven.models;

public class Response {
    private int responseId;
    private String description;
    private ResponseType responseType;
    private OrderFlight orderFlight;
    private Customer customer;

    public Response() {
    }

    public int getResponseId() {
        return responseId;
    }

    public void setResponseId(int responseId) {
        this.responseId = responseId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }

    public OrderFlight getOrderFlight() {
        return orderFlight;
    }

    public void setOrderFlight(OrderFlight orderFlight) {
        this.orderFlight = orderFlight;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Response{" +
                "responseId=" + responseId +
                ", description='" + description + '\'' +
                ", responseType=" + responseType +
                ", orderFlight=" + orderFlight +
                ", customer=" + customer +
                '}';
    }

    public String toStringMainData() {
        return "Response{" +
                "responseId=" + responseId +
                ", description='" + description + '\'' +
                ", responseType=" + responseType +
//                ", orderFlight=" + orderFlight +
//                ", customer=" + customer +
                '}';
    }

    public static ResponseBuilder builder() {
        return new ResponseBuilder();
    }

    public static class ResponseBuilder {

        Response response = new Response();

        public ResponseBuilder responseId(int id) {
            response.responseId = id;
            return this;
        }

        public ResponseBuilder description(String description) {
            response.description = description;
            return this;
        }

        public ResponseBuilder responseType(ResponseType type) {
            response.responseType = type;
            return this;
        }

        public ResponseBuilder orderFlight(OrderFlight orderFlight) {
            response.orderFlight = orderFlight;
            return this;
        }

        public ResponseBuilder customer(Customer customer) {
            response.customer = customer;
            return this;
        }

        public Response build() {
            return response;
        }

    }


}
