package com.epam.airlines_maven.models;

import java.sql.Date;
import java.util.List;

public class Flight {
    private int flightId;
    private int orderedSeats;
    private Date departure;
    private AirRoute route;
    private Aircraft aircraft;
    private List<Response> responses;

    public Flight() {
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public AirRoute getRoute() {
        return route;
    }

    public void setRoute(AirRoute route) {
        this.route = route;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    public int getOrderedSeats() {
        return orderedSeats;
    }

    public void setOrderedSeats(int orderedSeats) {
        this.orderedSeats = orderedSeats;
    }

    public boolean addOrderedSeat() {
        if ((aircraft == null) || (aircraft.getPassengerCapacity() <= orderedSeats)) {
            System.out.println("--------");
            return false;
        } else {
            this.orderedSeats++;
            return true;
        }
    }

    public String subtractOrderedSeat() {
        if (aircraft == null) {
            return "No aircraft set yet";
        } else {
            this.orderedSeats--;
            return "Your seat unordered from aircraft";
        }
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public void setResponses(List<Response> responses) {
        this.responses = responses;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightId=" + flightId +
                ", orderedSeats=" + orderedSeats +
                ", departure=" + departure +
                ", route=" + route +
                '}';
    }

    public String toStringWithAircraft() {
        return "Flight{" +
                "flightId=" + flightId +
                ", orderedSeats=" + orderedSeats +
                ", departure=" + departure +
                ", route=" + route +
                ", aircraft=" + aircraft +
                '}';
    }

//    public String toStringWithResponses() {
//        return "Flight{" +
//                "flightId=" + flightId +
//                ", orderedSeats=" + orderedSeats +
//                ", departure=" + departure +
//                ", route=" + route +
//                ", responses=" + responses +
//                '}';
//    }

    public static FlightBuilder builder() {
        return new FlightBuilder();
    }

    public static class FlightBuilder {

        Flight flight = new Flight();

        public FlightBuilder flightId(int id) {
            flight.flightId = id;
            return this;
        }

        public FlightBuilder orderedSeats(int seats) {
            flight.orderedSeats = seats;
            return this;
        }

        public FlightBuilder departure(Date date) {
            flight.departure = date;
            return this;
        }

        public FlightBuilder route(AirRoute airRoute) {
            flight.route = airRoute;
            return this;
        }

        public FlightBuilder aircraft(Aircraft aircraft) {
            flight.aircraft = aircraft;
            return this;
        }

        public FlightBuilder responses(List<Response> responses) {
            flight.responses = responses;
            return this;
        }

        public Flight build() {
            return flight;
        }
    }

}
