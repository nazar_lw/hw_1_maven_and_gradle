package com.epam.airlines_maven.models;

import java.sql.Date;

public class OrderFlight {
    private int orderId;
    private Date date;
    private Flight flight;
    private Customer customer;
    private Response response;

    public OrderFlight() {
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "OrderFlight{" +
                "orderId=" + orderId +
                ", date=" + date +
                ", flight=" + flight +
                ", customer=" + customer +
                ", response=" + response +
                '}';
    }

    public String toStringWithoutCustomerAndResponses() {
        return "OrderFlight{" +
                "orderId=" + orderId +
                ", date=" + date +
                ", flight=" + flight +
                '}';
    }

    public static OrderFlightBuilder builder() {
        return new OrderFlightBuilder();
    }

    public static class OrderFlightBuilder {

        OrderFlight orderFlight = new OrderFlight();

        public OrderFlightBuilder orderId(int id) {
            orderFlight.orderId = id;
            return this;
        }

        public OrderFlightBuilder date(Date date) {
            orderFlight.date = date;
            return this;
        }

        public OrderFlightBuilder flight(Flight flight) {
            orderFlight.flight = flight;
            return this;
        }

        public OrderFlightBuilder customer(Customer customer) {
            orderFlight.customer = customer;
            return this;
        }

        public OrderFlightBuilder response(Response response) {
            orderFlight.response = response;
            return this;
        }

        public OrderFlight build() {
            return orderFlight;
        }
    }

}
