package com.epam.airlines_maven.models;

public enum ResponseType {
    POSITIVE,
    NEGATIVE
}
