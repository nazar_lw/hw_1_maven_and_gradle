package com.epam.airlines_maven.models;

import java.util.List;

public class Customer {
    private int customerId;
    private String login;
    private String password;
    private String customerName;
    private boolean isEnabled;
    private List<Flight> flights;
    private List<OrderFlight> orders;

    public Customer() {
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public List<OrderFlight> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderFlight> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", customerName='" + customerName + '\'' +
                ", isEnabled=" + isEnabled +
//                ", flights=" + flights +
//                ", orders=" + orders +
                '}';
    }

    public static CustomerBuilder builder() {
        return new CustomerBuilder();
    }

    public static class CustomerBuilder {

        Customer customer = new Customer();

        public CustomerBuilder customerId(int id) {
            customer.customerId = id;
            return this;
        }

        public CustomerBuilder login(String login) {
            customer.login = login;
            return this;
        }

        public CustomerBuilder password(String pass) {
            customer.password = pass;
            return this;
        }

        public CustomerBuilder customerName(String name) {
            customer.customerName = name;
            return this;
        }

        public CustomerBuilder isEnabled(boolean isEnabled) {
            customer.isEnabled = isEnabled;
            return this;
        }

        public CustomerBuilder flights(List<Flight> flights) {
            customer.flights = flights;
            return this;
        }

        public CustomerBuilder orders(List<OrderFlight> orderFlights) {
            customer.orders = orderFlights;
            return this;
        }

        public Customer build() {
            return customer;
        }
    }
}
