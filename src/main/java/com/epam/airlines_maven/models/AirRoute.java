package com.epam.airlines_maven.models;

public class AirRoute {

    private int routeId;
    private String destination;

    public AirRoute() {
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AirRoute airRoute = (AirRoute) o;

        if (routeId != airRoute.routeId) return false;
        return destination != null ? destination.equals(airRoute.destination) : airRoute.destination == null;
    }

    @Override
    public int hashCode() {
        int result = routeId;
        result = 31 * result + (destination != null ? destination.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AirRoute{" +
                "routeId=" + routeId +
                ", destination='" + destination + '\'' +
                '}';
    }

    public static AirRouteBuilder builder() {
        return new AirRouteBuilder();
    }

    public static class AirRouteBuilder {

        AirRoute airRoute = new AirRoute();

        public AirRouteBuilder routeId(int id) {
            airRoute.routeId = id;
            return this;
        }

        public AirRouteBuilder destination(String destination) {
            airRoute.destination = destination;
            return this;
        }

        public AirRoute build() {
            return airRoute;
        }
    }

}
