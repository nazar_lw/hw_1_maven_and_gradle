package com.epam.airlines_maven.models.transitionobjects;

import com.epam.airlines_maven.models.Customer;

public class Authenticated {
    private Customer customer;
    private boolean isAuthenticated;

    public Authenticated(Customer customer, boolean isAuthenticated) {
        this.customer = customer;
        this.isAuthenticated = isAuthenticated;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public String toString() {
        return "Authenticated{" +
                "customer=" + customer +
                ", isAuthenticated=" + isAuthenticated +
                '}';
    }
}
