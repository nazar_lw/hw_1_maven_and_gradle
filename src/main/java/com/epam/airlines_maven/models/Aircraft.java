package com.epam.airlines_maven.models;

public class Aircraft {

    private int aircraftId;
    private String aircraftName;
    private int passengerCapacity;
    private Flight flight;

    public Aircraft() {
    }

    public int getAircraftId() {
        return aircraftId;
    }

    public void setAircraftId(int aircraftId) {
        this.aircraftId = aircraftId;
    }

    public String getAircraftName() {
        return aircraftName;
    }

    public void setAircraftName(String aircraftName) {
        this.aircraftName = aircraftName;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Aircraft aircraft = (Aircraft) o;

        if (aircraftId != aircraft.aircraftId) return false;
        if (passengerCapacity != aircraft.passengerCapacity) return false;
        if (aircraftName != null ? !aircraftName.equals(aircraft.aircraftName) : aircraft.aircraftName != null)
            return false;
        return flight != null ? flight.equals(aircraft.flight) : aircraft.flight == null;
    }

    @Override
    public int hashCode() {
        int result = aircraftId;
        result = 31 * result + (aircraftName != null ? aircraftName.hashCode() : 0);
        result = 31 * result + passengerCapacity;
        result = 31 * result + (flight != null ? flight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "aircraftId=" + aircraftId +
                ", aircraftName='" + aircraftName + '\'' +
                ", passengerCapacity=" + passengerCapacity +
//                ", flight=" + flight +
                '}';
    }

    public static AircraftBuilder builder() {
        return new AircraftBuilder();
    }

    public static class AircraftBuilder {

        Aircraft aircraft = new Aircraft();

        public AircraftBuilder aircraftId(int id) {
            aircraft.aircraftId = id;
            return this;
        }

        public AircraftBuilder aircraftName(String name) {
            aircraft.aircraftName = name;
            return this;
        }

        public AircraftBuilder passengerCapacity(int capacity) {
            aircraft.passengerCapacity = capacity;
            return this;
        }

        public AircraftBuilder flight(Flight flight) {
            aircraft.flight = flight;
            return this;
        }

        public Aircraft build() {
            return aircraft;
        }
    }
}
