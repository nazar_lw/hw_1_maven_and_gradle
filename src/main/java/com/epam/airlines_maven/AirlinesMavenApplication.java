package com.epam.airlines_maven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirlinesMavenApplication {

    public static void main(String[] args) {
        SpringApplication.run(AirlinesMavenApplication.class, args);
    }

}
