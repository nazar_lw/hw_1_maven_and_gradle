package com.epam.airlines_maven.serviceintegrationtests;

import com.epam.airlines_maven.dao.impl.FlightDAO;
import com.epam.airlines_maven.models.Flight;
import com.epam.airlines_maven.services.AircraftService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.sql.SQLException;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AircraftServiceTest {

    @MockBean
    private FlightDAO mockFlightDAO;
    @Autowired
    private AircraftService aircraftService;
    private Flight flight;

    @Before
    public void beforeClass() throws SQLException {
        flight = Flight.builder().
                flightId(111).
                departure(new Date(System.currentTimeMillis())).
                orderedSeats(60).build();
        when(mockFlightDAO.getByAircraftId(5)).thenReturn(flight);
        System.out.println("--- Mock the response from FlightDAO for getByAircraftId() method ---");
    }

    @Test
    public void deleteAircraftWhenItIsAssignedToFlight_Test() {
        System.out.println("--- Attempt to delete an aircraft that is assigned to the flight ---");
        Assert.assertEquals(" Response from service failed on deleting aircraft ",
                "AIRCRAFT is RESERVED to flight: " +
                flight.getFlightId()+"- assign another aircraft to this flight!",
                aircraftService.delete(5));
        System.out.println("--- Since the aircraft is reserved - it was not deleted ---");
    }

    @After
    public void after(){
        aircraftService = null;
        System.out.println("--- Resources flushed ---");
    }
}
