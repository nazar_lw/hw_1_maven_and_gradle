package com.epam.airlines_maven.serviceintegrationtests;

import com.epam.airlines_maven.dao.impl.CustomerDAO;
import com.epam.airlines_maven.models.Customer;
import com.epam.airlines_maven.services.CustomerService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.Base64;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {

    @MockBean
    private CustomerDAO mockCustomerDAO;
    @Autowired
    private CustomerService customerService;
    private final String password = "nazar";
    private Customer customer;

    @Before
    public void beforeClass() throws SQLException {
        customer = Customer.builder().
                login("nazar").
                password(password).
                customerName("Nazar").build();
        String encodedPass = Base64.getEncoder().encodeToString(password.getBytes());

        when(mockCustomerDAO.getPassByLogin(customer.getLogin())).thenReturn(encodedPass);
        when(mockCustomerDAO.changePassword(customer)).thenReturn(1);
        System.out.println("--- Mock the response from CustomerDAO for getPassByLogin() and changePassword() methods ---");
    }

    @Test
    public void changePasswordProvidingTrueAndWrongOldPasswords_Test() {
        System.out.println("--- Attempt to change the password while providing TRUE old password ---");
        Assert.assertEquals(" Response from service failed on changing the password " ,
                "Password was changed",
                customerService.changePassword(customer, password, "NewPassword"));
        System.out.println("--- Password was changed ---");

        System.out.println("--- Attempt to change the password while providing WRONG old password ---");
        Assert.assertEquals(" Response from service failed on changing the password " ,
                "Passwords DO NOT match!",
                customerService.changePassword(customer, "WrongOldPassword", "NewPassword"));
        System.out.println("--- Password was not changed ---");
    }

    @After
    public void after(){
        customerService = null;
        System.out.println("--- Resources flushed ---");
    }

}
