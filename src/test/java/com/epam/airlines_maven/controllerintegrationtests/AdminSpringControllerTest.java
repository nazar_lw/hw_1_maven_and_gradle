package com.epam.airlines_maven.controllerintegrationtests;

import com.epam.airlines_maven.AirlinesMavenApplication;
import com.epam.airlines_maven.models.AirRoute;
import com.epam.airlines_maven.services.AirRouteService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.SQLException;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = AirlinesMavenApplication.class
)
@AutoConfigureMockMvc
public class AdminSpringControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AirRouteService mockAirRouteService;

    @Before
    public void beforeClass() throws SQLException {
        AirRoute airRoute1 = AirRoute.builder().routeId(1).destination("Lviv-Kiev").build();
        AirRoute airRoute2 = AirRoute.builder().routeId(2).destination("Kiev-Odessa").build();
        when(mockAirRouteService.getAll()).thenReturn(Arrays.asList(airRoute1, airRoute2));
        System.out.println("--- Mock the response from AirRouteService for getAll() method ---");
    }

    @Test
    public void getAirRoutesPage_Test() throws Exception {
        mockMvc.perform(get("/airRoutes"))
                .andExpect(status().isOk())
                .andExpect(view().name("air_routes"))
                .andExpect(model().attribute("airRoutes", hasSize(2)))
                .andExpect(model().attribute("airRoutes", hasItem(
                        allOf(
                                hasProperty("routeId", is(1)),
                                hasProperty("destination", is("Lviv-Kiev"))
                        )
                )))
                .andExpect(model().attribute("airRoutes", hasItem(
                        allOf(
                                hasProperty("routeId", is(2)),
                                hasProperty("destination", is("Kiev-Odessa"))
                        )
                )));

        verify(mockAirRouteService, times(1)).getAll();
        verifyNoMoreInteractions(mockAirRouteService);
        System.out.println("--- Expected status code, view, model for url- '/airRoutes' attributes match to actual ---");
    }

    @After
    public void after(){
        mockAirRouteService = null;
        System.out.println("--- Resources flushed ---");
    }
}
