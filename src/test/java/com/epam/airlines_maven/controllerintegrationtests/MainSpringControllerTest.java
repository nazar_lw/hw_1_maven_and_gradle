package com.epam.airlines_maven.controllerintegrationtests;

import com.epam.airlines_maven.AirlinesMavenApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = AirlinesMavenApplication.class
)
@AutoConfigureMockMvc
public class MainSpringControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void saveCustomerWithWrongCredential_Test() throws Exception {
        System.out.println("--- Try to save a customer with login 'admin' ---");
        mockMvc.perform(post("/saveCustomer")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("login", "admin")
                .param("password", "nazar")
                .param("customerName", "Nazar")
        )
                .andExpect(status().isOk())
                .andExpect(view().name("registration"))
                .andExpect(model().attribute("resultOfRegistration",
                        "Login is not UNIQUE!"));
        System.out.println("--- Expected status code, view, model for url- '/saveCustomer' attributes match to actual ---");
    }

    @Test
    public void logInWithEmptyCredential_Test() throws Exception {
        System.out.println("--- Try to log in with empty credentials ---");
        mockMvc.perform(post("/loginCombination")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", "")
                .param("password", "nazar")
        )
                .andExpect(status().isOk())
                .andExpect(view().name("login"))
                .andExpect(model().attribute("resultOfLogination",
                        "Credentials Can not be empty Strings!!"));
        System.out.println("--- Expected status code, view, model for url- '/loginCombination' attributes match to actual ---");
    }
}
