package com.epam.airlines_maven.daointegrationtests;

import com.epam.airlines_maven.dao.impl.CustomerDAO;
import com.epam.airlines_maven.models.Customer;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerDaoTest {

    private static CustomerDAO customerDAO;

    @BeforeClass
    public static void beforeClass() throws SQLException {
        customerDAO = new CustomerDAO();
        Customer customer1 = Customer.builder().
                login("nazar1").
                password("nazar1").
                customerName("Nazar").build();
        customerDAO.save(customer1);
        Customer customer2 = Customer.builder().
                login("yura1").
                password("yura1").
                customerName("Yura").build();
        customerDAO.save(customer2);
        Customer customer3 = Customer.builder().
                login("petro1").
                password("petro1").
                customerName("Petro").build();
        customerDAO.save(customer3);
        System.out.println("---- DB populated ----");
    }

    @Test
    public void saveAndDeleteCustomers_Test() throws SQLException {

        Assert.assertTrue("Customer with such login does not exists", customerDAO.existsByLogin("nazar1"));
        Assert.assertTrue("Customer with such login does not exists", customerDAO.existsByLogin("yura1"));
        Assert.assertTrue("Customer with such login does not exists", customerDAO.existsByLogin("petro1"));
        System.out.println("--- All customers exist in the DB ---");

        Assert.assertNotEquals( "", customerDAO.getPassByLogin("yura1"));
        System.out.println("--- Selected customer has a password ---");

        int sizeBeforeDelete = customerDAO.getAll().size();
        customerDAO.delete(customerDAO.getByLogin("nazar1").getCustomerId());
        customerDAO.delete(customerDAO.getByLogin("yura1").getCustomerId());
        customerDAO.delete(customerDAO.getByLogin("petro1").getCustomerId());
        int sizeAfterDelete = customerDAO.getAll().size();

        Assert.assertFalse("Customer with such login still exists", customerDAO.existsByLogin("nazar1"));
        Assert.assertFalse("Customer with such login still exists", customerDAO.existsByLogin("yura1"));
        Assert.assertFalse("Customer with such login still exists", customerDAO.existsByLogin("petro1"));
        System.out.println("--- Customers do not exist in the DB ---");

        Assert.assertEquals("Number of customers has not changed",3, sizeBeforeDelete-sizeAfterDelete);
        System.out.println(" --- DB cleaned --- ");
    }

    @AfterClass
    public static void after(){
        customerDAO = null;
        System.out.println("--- Resources flushed ---");
    }

}
